# Extra 

## openssl commands

### Extract the certs and private key from pkcs12

Extract and remove password

```
openssl pkcs12 -in my-secret.p12 -nodes -info -out my-secret.pem 
```

Use editor to get the certificate in my-cert.pem

Get the public key from the certificate

```
openssl x509 -in my-cert.pem -pubkey -out my-pubkey.pem -noout
```

## Swagger codegen

Run from docker container

Generate client for warehouse 13.

The command writes the files to ./out/go. You can changes this with the -o option. Bear in mind that you should always use the /local prefix.

- change -o to point to the directory to write the generated files to
- add the --user option to ensure that you are the owner of the files ```--user $(id -u):$(id -g)```

```
docker run --rm --user $(id -u):$(id -g) -v ${PWD}:/local swaggerapi/swagger-codegen-cli-v3 \
  generate \
  -i https://w13.isharetest.net/swagger/v1.9/swagger.json \
  -l go \
  -o /local/out/go
```

```
docker run --rm --user $(id -u):$(id -g) -v ${PWD}:/local swaggerapi/swagger-codegen-cli-v3 \
  generate \
  -i https://scheme.isharetest.net/swagger/v1.9/swagger.json \
  -l go \
  -o /local/out/go
```

## iShare notes

All iShare services need to offer an access token service. Access tokens have a validity period and should be used for 
all calls to the iShare service within that period. When an access token goes stale, the client needs to request a new 
access token.

```
    func ExecuteServiceRequest() {
        if ! access token is valid {
            refresh token
        }
        prepare service request with access token
        do service request
    }
```

Because each iShare client needs to perform these actions we use a struct with methods for this functionality.

## TODO

| Task# | Name | Description | Status | 
|-------|------|-------------|--------|
| T001 | Party | Create Party object for an iShare participant | Open |
| T002 | Common | Create methods for issuing the common requests | Open | 


## Object Model

### Party

A party represents an iShare participant. It has an EORI, and a client cert and key.

### Client

A client is the combination of a client party and one or more server parties. 

### AccessToken

An access token is token that gives access to services of a server. The client retrieves access tokens from a service provider and a token has a limited validity.

### AccessTokenClient

An access token client retrieves and manages access tokens on behalve of a client to access a server. It is responsible for retrieving and refreshing tokens. It uses the information in the token to determine if it needs to refresh the token. I also allows the user of this client to force a refresh.

### Server 

A server is a party that offers a service. It has an EORI and an Endpoint URL. Each server needs to support the common iShare methods for providing an access token the capabilities of the service.

### Godoc

```
godoc -http localhost:6060 -index -v
```