# iShare in Go 

Go package for a service client that accesses services using iShare for authorization. 

[iShare for developers](https://dev.ishareworks.org/)

The implementation follows the steps presented in the iShare portal
1. [create a valid iShare jwt token](https://dev.ishareworks.org/introduction/jwt.html#refjwt)
1. obtain an iShare access token
2. access the service with the access token

## Status

1. signed token is valid according to jwt.io


