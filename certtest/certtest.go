package certtest

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

var (
	ErrBadX5CEntry = errors.New("bad x5c entry")
)

// findLeaf finds the cert that is not an issuer.
// func findLeaf(certs map[string]*x509.Certificate) string {
// 	// Build a hashmap with issuers only.
// 	var issuers = make(map[string]struct{})
// 	for _, c := range certs {
// 		key := c.Issuer.ToRDNSequence().String()
// 		// Prevent he root to be written twice.
// 		if _, ok := issuers[key]; !ok {
// 			issuers[c.Issuer.ToRDNSequence().String()] = struct{}{}
// 		}
// 	}
// 	// Find cert that is not in issuers.
// 	for sk := range certs {
// 		if _, ok := issuers[sk]; !ok {
// 			return sk
// 		}
// 	}
// 	return ""
// }

// orderByLeaf builds an array that contains the path from the leaf to the root.
// func orderByLeaf(leaf string, certs map[string]*x509.Certificate) ([]*x509.Certificate, error) {
// 	res := make([]*x509.Certificate, 0, len(certs))
// 	for key := leaf; len(res) < len(certs); {
// 		c := certs[key]
// 		res = append(res, c)
// 		key = c.Issuer.ToRDNSequence().String()
// 	}
// 	root := res[len(res)-1]
// 	if root.Subject.ToRDNSequence().String() != root.Issuer.ToRDNSequence().String() {
// 		return nil, ErrNoRootFound
// 	}
// 	return res, nil
// }

// encodeCerts takes the DER encoded certs and returns them in base64 encoded string array.
// func encodeCerts(certs []*x509.Certificate) []string {
// 	var res []string
// 	for _, c := range certs {
// 		res = append(res, base64.StdEncoding.EncodeToString(c.Raw))
// 	}
// 	return res
// }

// parseCerts and orders the found certs leaf first.
// func parseCerts(blocks []*pem.Block) ([]*x509.Certificate, error) {
// 	var certs = make(map[string]*x509.Certificate)
// 	for _, b := range blocks {
// 		if c, err := x509.ParseCertificate(b.Bytes); err == nil {
// 			certs[c.Subject.ToRDNSequence().String()] = c
// 		}
// 	}
// 	if len(certs) < 1 {
// 		return nil, ErrNoCertsFound
// 	}
// 	leaf := findLeaf(certs)
// 	if leaf == "" {
// 		return nil, ErrNoLeafFound
// 	}
// 	return orderByLeaf(leaf, certs)
// }

// func loadPemBlocks(b []byte) []*pem.Block {
// 	var p *pem.Block
// 	var res []*pem.Block

// 	next := func() {
// 		p, b = pem.Decode(b)
// 	}

// 	for next(); p != nil; next() {
// 		if p.Type == "CERTIFICATE" {
// 			res = append(res, p)
// 		}
// 	}
// 	return res
// }

// ParseX5CFile parses the content of the pem encoded file and
// returns the x5c value for the iShare jwt access token.
// func ParseX5CFile(file string) ([]string, error) {
// 	f, err := os.Open(file)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer f.Close()
// 	b, err := ioutil.ReadAll(f)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return ParseX5CPem(b)
// }

// ParseX5C parses the pem encoded certificates and
// returns the x5c value for the iShare jwt access token.
// func ParseX5CPem(data []byte) ([]string, error) {
// 	blocks := loadPemBlocks(data)
// 	if blocks == nil {
// 		return nil, ErrNoPemBlocksFound
// 	}
// 	certs, err := parseCerts(blocks)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return encodeCerts(certs), nil
// }

// func ParseCertBase64(data string) (*x509.Certificate, error) {
// 	var fixed bool
// 	var prefix = []byte("-----BEGIN")
// 	res, err := base64.StdEncoding.DecodeString(data)
// 	if err != nil {
// 		return nil, err
// 	}
// 	// Check if the bytes are in pem format (iShare error).
// 	if fixed = bytes.Compare(res[:len(prefix)], prefix) == 0; fixed {
// 		p, _ := pem.Decode(res)
// 		if p == nil {
// 			return nil, ErrBadX5CEntry
// 		}
// 		res = p.Bytes
// 	}
// 	cert, err := x509.ParseCertificate(res)
// 	if err != nil {
// 		return nil, err
// 	}
// 	if fixed {
// 		// WARNING
// 		Log.Warnf("ParseCertBase64: fixed PEM entry in x5c for cert: %s", cert.Subject.String())
// 	} else {
// 		// INFO
// 		Log.Infof("ParseCertBase64: parsed correct PEM entry in x5c for cert: %s", cert.Subject.String())
// 	}
// 	return cert, nil
// }

// func CertPoolFromX5C(x5c []string) (firstCert *x509.Certificate, pool *x509.CertPool, err error) {
// 	pool = x509.NewCertPool()
// 	for _, s := range x5c {
// 		cert, err := ParseCertBase64(s)
// 		if err != nil {
// 			Log.Errorf("error parsing x5c string: %s, string %s", err, s)
// 			return nil, nil, err
// 		}
// 		if firstCert == nil {
// 			firstCert = cert
// 		}
// 		pool.AddCert(cert)
// 	}
// 	return firstCert, pool, nil
// }

func loadCertFromPemFile(certFile string) (*x509.Certificate, error) {
	f, err := os.Open(certFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	p, _ := pem.Decode(b)
	if p == nil {
		return nil, errors.New("no pem block found")
	}
	cert, err := x509.ParseCertificate(p.Bytes)
	if err != nil {
		return nil, err
	}
	return cert, nil
}

func loadCertsFromPemFiles(files []string) ([]*x509.Certificate, error) {
	var res = make([]*x509.Certificate, len(files))

	for i, f := range files {
		c, err := loadCertFromPemFile(f)
		if err != nil {
			return nil, fmt.Errorf("error loading file %s: %w", f, err)
		}
		res[i] = c
	}
	return res, nil
}
