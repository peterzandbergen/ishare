package certtest

import (
	"crypto/x509"
	"testing"
)

func TestGoodCertChain(t *testing.T) {
	// SKIP
	t.SkipNow()
	
	const (
		certFile          = "/home/peza/DevProjects/ishare/resources/testcerts/testcert.www.logius.nl.pem"
		intermediate1File = "/home/peza/DevProjects/ishare/resources/testcerts/intermediate1.QuoVadis PKIoverheid Server CA 2020.pem"
		intermediate2File = "/home/peza/DevProjects/ishare/resources/testcerts/intermediate2.Staat der Nederlanden Domein Server CA 2020.pem"
		rootFile1         = "/home/peza/DevProjects/ishare/resources/testcerts/staat_der_nederlanden_ev_root_ca.pem"
		rootFile2         = "/home/peza/DevProjects/ishare/resources/testcerts/staat_der_nederlanden_root_ca_-_g2.pem"
		rootFile3         = "/home/peza/DevProjects/ishare/resources/testcerts/staat_der_nederlanden_root_ca_-_g3.pem"
	)

	// Load Cert
	cert, err := loadCertFromPemFile(certFile)
	if err != nil {
		t.Fatalf("%s", err)
	}
	// t.Logf("%v", cert)
	// Load intermediates
	ims, err := loadCertsFromPemFiles([]string{intermediate1File, intermediate2File})
	if err != nil {
		t.Logf("%v", ims)
		t.Fatalf("%s", err)
	}
	imspool := x509.NewCertPool()
	for _, c := range ims {
		imspool.AddCert(c)
	}
	// Verify using system root
	vopts := x509.VerifyOptions{
		Intermediates: imspool,
	}
	chain, err := cert.Verify(vopts)
	_ = chain
	if err != nil {
		t.Logf("verify failed for cert.CommonName %s", cert.Subject.CommonName)
		t.Fatalf("%s", err)
	}
}

func TestBadCertChain(t *testing.T) {
	const (
		certFile          = "/home/peza/DevProjects/ishare/resources/testcerts/testcert.www.logius.nl.pem"
		intermediate1File = "/home/peza/DevProjects/ishare/resources/testcerts/intermediate1.QuoVadis PKIoverheid Server CA 2020.pem"
		intermediate2File = "/home/peza/DevProjects/ishare/resources/testcerts/intermediate2.Staat der Nederlanden Domein Server CA 2020.pem"
		rootFile1         = "/home/peza/DevProjects/ishare/resources/testcerts/staat_der_nederlanden_ev_root_ca.pem"
		rootFile2         = "/home/peza/DevProjects/ishare/resources/testcerts/staat_der_nederlanden_root_ca_-_g2.pem"
		rootFile3         = "/home/peza/DevProjects/ishare/resources/testcerts/staat_der_nederlanden_root_ca_-_g3.pem"
	)

	// Load Cert
	cert, err := loadCertFromPemFile(certFile)
	if err != nil {
		t.Fatalf("%s", err)
	}
	t.Logf("%v", cert)
	// Load intermediates
	ims, err := loadCertsFromPemFiles([]string{intermediate1File, intermediate2File})
	if err != nil {
		t.Logf("%v", ims)
		t.Fatalf("%s", err)
	}
	imspool := x509.NewCertPool()
	for _, c := range ims {
		imspool.AddCert(c)
	}
	// Load intermediates
	rootcajava, err := loadCertsFromPemFiles([]string{rootFile1, rootFile2, rootFile3})
	if err != nil {
		t.Logf("%v", rootcajava)
		t.Fatalf("%s", err)
	}
	// Create java root pool
	javaroots := x509.NewCertPool()
	for _, c := range rootcajava {
		imspool.AddCert(c)
	}
	// Verify using java root
	javavopts := x509.VerifyOptions{
		Intermediates: imspool,
		Roots:         javaroots,
	}
	javachain, err := cert.Verify(javavopts)
	if err == nil {
		t.Fatalf("error: verification passed unexpectately")
	}
	_ = javachain
}
