package main

import (
	"context"
	"os"

	"gitlab.com/peterzandbergen/ishare/internal/actions"
)

func run(args []string) error {
	app := actions.NewApp()
	err := app.RunContext(context.Background(), args)
	logger := app.Logger()
	if err != nil {
		logger.Info("run failed", "err_string", err.Error())
	}
	if err != nil {
		return err
	}
	return nil
}

func main() {
	run(os.Args)
}
