package main

import "testing"

func TestModalityCapabilities(t *testing.T) {
	args := []string{
		"isharecli",
		`get`,
		"--server-eori=EU.EORI.NL000000000",
		`--service-url=https://bctn.modalityservices.nl:50333/capabilities`,
		`--token-url=https://bctn.modalityservices.nl:50333/oauth2.0/token`,
		`--client-eori=EU.EORI.NL51197073`,
		`--client-key=/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem`,
		`--client-cert=/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem`,
		`--client-cacerts=/home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem`,
		`--client-roots=/home/peza/Documents/iShare/iShareTestKey2/root.pem`,
	}
	err := run(args)
	if err != nil {
		t.Errorf("error: %s", err)
	}
}

func TestGetTrustedList(t *testing.T) {
	args := []string{
		"isharecli",
		`trusted-list`,
		"--server-eori=EU.EORI.NL000000000",
		"--server-host=https://scheme.isharetest.net",
		`--client-eori=EU.EORI.NL51197073`,
		`--client-key=/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem`,
		`--client-cert=/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem`,
		`--client-cacerts=/home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem`,
		`--client-roots=/home/peza/Documents/iShare/iShareTestKey2/root.pem`,
		`--help`,
	}
	err := run(args)
	if err != nil {
		t.Errorf("error: %s", err)
	}
}
