#!/bin/bash

VERSION="$(date --iso-8601=seconds) $(git rev-parse --short HEAD)"

# CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main -ldflags "-X ishare/internal/actions.programVersion=$VERSION" ishare/cmd/isharecli
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main \
    -ldflags="-X 'ishare/internal/actions.programVersion=$VERSION'" \
    ishare/cmd/isharecli
