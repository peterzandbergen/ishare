/*
Package ishare implements the iShare scheme for oauth2. 
iShare is a scheme for securing and controlling access to 
APIs using OAuth and JWT tokens with x5c certificates.
On top of these technologies iShare implements 
services for discovery and delegation. 

See for more information https://www.ishareworks.org/en and 
https://dev.ishareworks.org/ 
  

*/
package ishare