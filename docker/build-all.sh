#!/bin/bash

WORKDIR=$(dirname $0)

$WORKDIR/build.sh
$WORKDIR/build-alpine.sh
$WORKDIR/build-scratch.sh
