#!/bin/bash

cd $(dirname $0)/..

DOCKER_FILE=docker/isharecli/Dockerfile-alpine
IMAGE_NAME=isharecli
DOCKER_REG=docker.io/peterzandbergen
IMAGE_TAG=$(git rev-parse --short HEAD)
VERSION="$(date --iso-8601=seconds) $(git rev-parse --short HEAD)"

echo Building image from $(pwd)

podman build --file $DOCKER_FILE \
    --tag ${IMAGE_NAME}:alpine \
    --tag ${IMAGE_NAME}:${IMAGE_TAG}-alpine \
    --tag $DOCKER_REG/${IMAGE_NAME}:alpine \
    --tag $DOCKER_REG/${IMAGE_NAME}:${IMAGE_TAG}-alpine \
    --build-arg "VERSION=${VERSION}" \
     .
