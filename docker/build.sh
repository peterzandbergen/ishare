#!/bin/bash

cd $(dirname $0)/..

DOCKER_FILE=docker/isharecli/Dockerfile
IMAGE_NAME=isharecli
DOCKER_REG=docker.io/peterzandbergen
IMAGE_TAG=$(git rev-parse --short HEAD)
VERSION="$(date --iso-8601=seconds) $(git rev-parse --short HEAD)"

echo Building image from $(pwd)

podman build --file $DOCKER_FILE \
    --tag ${IMAGE_NAME}:latest \
    --tag ${IMAGE_NAME}:bullseye \
    --tag ${IMAGE_NAME}:${IMAGE_TAG} \
    --tag ${IMAGE_NAME}:${IMAGE_TAG}-bullseye \
    --tag $DOCKER_REG/${IMAGE_NAME}:latest \
    --tag $DOCKER_REG/${IMAGE_NAME}:bullseye \
    --tag $DOCKER_REG/${IMAGE_NAME}:${IMAGE_TAG} \
    --tag $DOCKER_REG/${IMAGE_NAME}:${IMAGE_TAG}-bullseye \
    --build-arg "VERSION=${VERSION}" \
     .
