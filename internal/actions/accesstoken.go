package actions

import (
	"context"
	"fmt"
	"sort"

	"github.com/urfave/cli/v2"

	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"
)

type AccessTokenAction struct {
	*GlobalAction

	serverEori string
	tokenURL   string
}

func (a *AccessTokenAction) configIshareConfig() (*isharecredentials.Config, error) {
	// Load the certs and key.
	cfg := &isharecredentials.Config{
		TokenURL:   a.tokenURL,
		ClientEORI: a.clientEORI,
		Cert:       a.lClientCert,
		ServerEORI: a.serverEori,
		Key:        a.lClientKey,
		CACerts:    a.lClientCACerts,
		RootCerts:  a.lClientRoots,
	}
	return cfg, nil
}

func (a *AccessTokenAction) Action(appCtx *cli.Context) error {
	if err := a.GlobalAction.Action(appCtx); err != nil {
		return fmt.Errorf("global action failed: %w", err)
	}
	cfg, err := a.configIshareConfig()
	if err != nil {
		return fmt.Errorf("config ishare failed: %w", err)
	}

	if a.dryRun {
		return nil
	}

	tk, err := a.getToken(cfg)
	if err != nil {
		return fmt.Errorf("get token failed: %w", err)
	}
	fmt.Printf("access token jwt\n-----BEGIN JTW ACCESS TOKEN-----\n%s\n-----END JTW ACCESS TOKEN-----\n", tk)
	return nil
}

func (a *AccessTokenAction) getToken(cfg *isharecredentials.Config) (string, error) {
	// Create a token service.
	tk, err := cfg.Token(context.Background())
	if err != nil {
		return "", err
	}
	return tk.AccessToken, nil
}

func NewAccessTokenCommand(global *GlobalAction) *cli.Command {
	cfg := AccessTokenAction{
		GlobalAction: global,
	}
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:        "server-eori",
			Destination: &cfg.serverEori,
			Required:    true,
			Usage:       "ishare server eori, the eori of the server to communicate with",
		},
		&cli.StringFlag{
			Name:        "token-url",
			Destination: &cfg.tokenURL,
			Required:    true,
			Usage:       "token endpoint url for getting the token from",
			EnvVars: []string{"ISHARE_TOKEN_URL"},
		},
	}
	cmd := &cli.Command{
		Name:        "token",
		Description: "Retrieves a token and writes it to stdout. \nWARNING: This token contains sensitive information so handle with care.",
		// UsageText:   "request and access token from the endpoint for the server eori",
		ArgsUsage:   "args usage",
		Usage:       "get an access token",
		Action:      cfg.Action,
		Flags:       append(cfg.GlobalAction.Flags(), flags...),
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd
}
