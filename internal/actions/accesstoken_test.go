package actions

import (
	"context"
	"testing"
)

const (
	cbsEori = "EU.EORI.NL51197073"
	certCBS = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem"
	keyCBS  = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem"

	caCertChain = "/home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem"
	rootCert    = "/home/peza/Documents/iShare/iShareTestKey2/root.pem"

	schemeOwnerHost            = "https://scheme.isharetest.net"
	schemeOwnerTokenURI        = "https://scheme.isharetest.net/connect/token"
	schemeOwnerCapabilitiesURI = "https://scheme.isharetest.net/capabilities"
	schemeOwnerEori            = "EU.EORI.NL000000000"

	warehouse13Host = "https://w13.isharetest.net"
	warehouse13Eori = "EU.EORI.NL000000003"

	authorizationRegistryHost = "https://ar.isharetest.net"
	authorizationRegistryEori = "EU.EORI.NL000000004"

	bananaCoEori     = "EU.EORI.NL000000005"
	bananaCoHost     = "https://banana.isharetest.net"
	bananaCoTokenUri = "https://banana.isharetest.net/connect/token"

	awesomeWidgetsHost = "https://awesome.isharetest.net"
	awesomeWidgetsEori = "EU.EORI.NL000000002"
	awesomeWidgetsTokenUri = "https://awesome.isharetest.net/connect/token"
)

func TestTokenAction1(t *testing.T) {
	args := []string{"isharecli", "token",
		"--client-eori", cbsEori,
		"--client-cert", certCBS,
		"--client-key", keyCBS,
		"--client-cacerts", caCertChain,
		"--client-roots", rootCert,
		"--token-url", schemeOwnerTokenURI,
		"--server-eori", schemeOwnerEori,
	}

	err := NewApp().RunContext(context.Background(), args)
	if err != nil {
		t.Errorf("error: %s", err)
	}
}

func TestTokenAction2(t *testing.T) {
	args := []string{"isharecli", "token",
		"--client-eori", cbsEori,
		"--client-cert", certCBS,
		"--client-key", keyCBS,
		"--client-cacerts", caCertChain,
		"--client-roots", rootCert,
		"--token-url", bananaCoTokenUri,
		"--server-eori", bananaCoEori,
	}

	err := NewApp().RunContext(context.Background(), args)
	if err != nil {
		t.Errorf("error: %s", err)
	}
}
