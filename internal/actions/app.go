package actions

import (
	"context"
	"sort"

	"github.com/go-logr/logr"
	"github.com/urfave/cli/v2"
)

type App struct {
	*cli.App

	global *GlobalAction
}

func NewApp() *App {
	return newApp()
}

func (a *App) Logger() logr.Logger {
	return a.global.Logger()
}

func (a *App) RunContext(ctx context.Context, args []string) error {
	return a.App.RunContext(ctx, args)
}

// newApp creates the App.
func newApp() *App {
	global := &GlobalAction{name: "created by app"}
	app := &App{
		App: &cli.App{
			Name:        "ishare CLI",
			Usage:       "an iShare oauth cli",
			Description: "description",
			// UsageText: "ishareadapter [global options]",
			Commands: []*cli.Command{
				NewServeCommand(global),
				// NewVerifyCommand(global),
				NewAccessTokenCommand(global),
				NewAssertionCommand(global),
				NewGetCommand(global),
				NewTrustedListCommand(global),
				NewPostCommand(global),
				NewVersionCommand(),
			},
			EnableBashCompletion: true,
		},
		global: global,
	}
	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))
	return app
}
