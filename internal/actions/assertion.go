package actions

import (
	"fmt"
	"sort"

	"github.com/urfave/cli/v2"

	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"
)

type AssertionAction struct {
	*GlobalAction

	serverEori string
	tokenURL   string
}

func NewAssertionCommand(global *GlobalAction) *cli.Command {
	cfg := AssertionAction{
		GlobalAction: global,
	}
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:        "server-eori",
			Destination: &cfg.serverEori,
			Required:    true,
			Usage:       "ishare server eori, the eori of the server this assertion is for",
			EnvVars:     []string{"ISHARE_SERVER_EORI"},
		},
	}
	cmd := &cli.Command{
		Name:        "assertion",
		Description: "Creates an ishare assertion and writes it to stdout. \nWARNING: This token contains sensitive information so handle with care.",
		Usage:       "create and show an iShare client assertion",
		ArgsUsage:   "args usage",
		Action:      cfg.Action,
		Flags:       append(cfg.GlobalAction.Flags(), flags...),
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd
}

func (a *AssertionAction) Action(appCtx *cli.Context) error {
	if err := a.GlobalAction.Action(appCtx); err != nil {
		return err
	}
	cfg, err := a.newIshareConfig()
	if err != nil {
		return err
	}

	if a.dryRun {
		return nil
	}

	assertion, err := cfg.Assertion()
	if err != nil {
		return err
	}
	fmt.Printf("assertion jwt\n-----BEGIN JTW ASSERTION-----\n%s\n-----END JTW ASSERTION-----\n", assertion)
	return nil
}

func (a *AssertionAction) newIshareConfig() (*isharecredentials.Config, error) {
	// Load the certs and key.
	cfg := &isharecredentials.Config{
		TokenURL:   a.tokenURL,
		ClientEORI: a.clientEORI,
		Cert:       a.lClientCert,
		ServerEORI: a.serverEori,
		Key:        a.lClientKey,
		CACerts:    a.lClientCACerts,
		RootCerts:  a.lClientRoots,
		TokenBackdate: a.backdate,
	}
	return cfg, nil
}
