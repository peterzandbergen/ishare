package actions

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/peterzandbergen/ishare/pkg/claims"
)

type fakeClaims map[string]interface{}

func (f fakeClaims) GetClaims() claims.Claims {
	return claims.Claims{}
}

func isTokenResponse(any interface{}) (tokenString string, err error) {
	// Cast to map.
	obj, ok := any.(map[string]interface{})
	if !ok {
		return "", fmt.Errorf("any is not an object, may be an array")
	}

	if l := len(obj); l != 1 {
		// Not a token, return the complete response.
		return "", fmt.Errorf("expected one property, found %d", len(obj))
	}
	// One property found. Check if it could be a token.
	for k := range obj {
		v, ok := obj[k].(string)
		if !ok {
			return "", fmt.Errorf("property %s of type %s, expected string", k, reflect.TypeOf(obj[k]))
		}
		if strings.Contains(strings.ToLower(v), "token") {
			return v, nil
		}
	}
	return "", fmt.Errorf("no token found")
}
