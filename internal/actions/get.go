package actions

import (
	"fmt"
	"net/http"
	"sort"

	"github.com/urfave/cli/v2"
)

type GetAction struct {
	*HTTPRequestAction
}

// NewGetCommand create a GetAction command.
//
// GetAction retrieves the data for the given url and sends
// the payload to stdout.
// Parameters are:
// 	- server-eori
//  - server uri
//  - service endpoint, full uri
func NewGetCommand(global *GlobalAction) *cli.Command {
	cfg := GetAction{
		HTTPRequestAction: &HTTPRequestAction{
			GlobalAction: global,
		},
	}
	cmd := &cli.Command{
		Name:        "get",
		Description: "Performs GET on the given service endpoint.",
		UsageText:   "ishare get [command options]",
		ArgsUsage:   "",
		Usage:       "do GET request",
		Action:      cfg.Action,
		Flags:       cfg.HTTPRequestAction.Flags(),
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd
}

func (a *GetAction) Action(appCtx *cli.Context) error {
	if err := a.HTTPRequestAction.Action(appCtx); err != nil {
		return err
	}
	req, err := http.NewRequest("GET", a.serviceURL, nil)
	if err != nil {
		return fmt.Errorf("error creating GET request for %s: %w", a.serviceURL, err)
	}

	return a.DoRequest(req)
}