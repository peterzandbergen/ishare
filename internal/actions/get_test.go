package actions

import (
	"context"
	"fmt"
	"testing"
)

func TestGet(t *testing.T) {

	args := []string{"isharecli", "get",
		"--client-eori", cbsEori,
		"--client-cert", certCBS,
		"--client-key", keyCBS,
		"--client-cacerts", caCertChain,
		"--client-roots", rootCert,
		"--token-url", bananaCoTokenUri,
		"--server-eori", bananaCoEori,
		"--service-url", fmt.Sprintf("%s/me", bananaCoHost),
	}

	err := NewApp().RunContext(context.Background(), args)
	if err != nil {
		t.Errorf("error: %s", err)
	}
}

func TestGetAwesome(t *testing.T) {

	args := []string{"isharecli", "get",
		"--client-eori", cbsEori,
		"--client-cert", certCBS,
		"--client-key", keyCBS,
		"--client-cacerts", caCertChain,
		"--client-roots", rootCert,
		"--token-url", awesomeWidgetsTokenUri,
		"--server-eori", awesomeWidgetsEori,
		"--service-url", fmt.Sprintf("%s/me", awesomeWidgetsHost),
	}

	err := NewApp().RunContext(context.Background(), args)
	if err != nil {
		t.Errorf("error: %s", err)
	}
}

