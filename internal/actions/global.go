package actions

import (
	"crypto/rsa"
	"crypto/x509"
	"fmt"
	"strings"

	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"github.com/urfave/cli/v2"
	"go.uber.org/zap"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
)

type GlobalAction struct {
	clientCert    string
	clientKey     string
	clientCACerts string
	clientRoots   string
	clientEORI    string
	// number of seconds to backdate the assertion
	backdate      int

	cliDebug bool
	dryRun   bool
	logEnv   string

	// Loaded certs and keys
	lClientCert    *x509.Certificate
	lClientKey     *rsa.PrivateKey
	lClientCACerts []*x509.Certificate
	lClientRoots   *x509.CertPool
	lAdapterCert   *x509.Certificate
	lAdapterKey    *rsa.PrivateKey

	// Logger
	logger logr.Logger

	// For testing.
	name string
}

func (g *GlobalAction) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "client-cert",
			Destination: &g.clientCert,
			// Value:   ".",
			Usage:    "ishare client cert for signing JWT tokens",
			EnvVars:  []string{"ISHARE_CLIENT_CERT"},
			Required: true,
		},
		&cli.StringFlag{
			Name:        "client-key",
			Destination: &g.clientKey,
			// Value:   ".",
			Usage:    "ishare client key for signing JWT tokens",
			EnvVars:  []string{"ISHARE_CLIENT_KEY"},
			Required: true,
		},
		&cli.StringFlag{
			Name:        "client-cacerts",
			Destination: &g.clientCACerts,
			Usage:       "ishare client cacerts for checking certificates and JWT",
			EnvVars:     []string{"ISHARE_CLIENT_CACERTS"},
			Required:    false,
		},
		&cli.StringFlag{
			Name:        "client-roots",
			Destination: &g.clientRoots,
			// Value:   ".",
			Usage:   "ishare client root certificates for checking certificates and JWT",
			EnvVars: []string{"ISHARE_CLIENT_ROOTS"},
		},
		&cli.StringFlag{
			Name:        "client-eori",
			Destination: &g.clientEORI,
			// Value:   ".",
			Usage:    "ishare client eori of the calling party",
			EnvVars:  []string{"ISHARE_CLIENT_EORI"},
			Required: true,
		},
		&cli.BoolFlag{
			Name:        "cli-debug",
			Destination: &g.cliDebug,
			Value:       false,
			Usage:       "dump cli parameters",
		},
		&cli.BoolFlag{
			Name:        "dry-run",
			Destination: &g.dryRun,
			Value:       false,
			Usage:       "perform all checks but not the actual action",
		},
		&cli.StringFlag{
			Name:        "log-env",
			Destination: &g.logEnv,
			Value:       "prod",
			Usage:       `logging environment ["dev", "prod"], sets the log output format`,
			EnvVars:     []string{"ISHARE_LOG_ENV"},
		},
		&cli.IntFlag{
			Name:        "backdate",
			Destination: &g.backdate,
			Value:       10,
			Usage:       `number of seconds to backdate the assertion`,
			EnvVars:     []string{"ISHARE_BACKDATE"},
		},
	}
}

// Action performs the GlobalAction initialization.
func (g *GlobalAction) Action(appCtx *cli.Context) error {
	// set the logger.
	g.setLogger(g.logEnv)
	// Load the certs.
	if err := g.LoadCertsAndKeys(); err != nil {
		return fmt.Errorf("error loading certs and keys: %w", err)
	}
	g.name = g.name + " Action called"
	return nil
}

func (g *GlobalAction) String() string {
	return fmt.Sprintf("clientCert=%s, clientKey=%s, clientCACerts=%s, clientRoots=%s, clientEORI=%s",
		g.clientCert,
		g.clientKey,
		g.clientCACerts,
		g.clientRoots,
		g.clientEORI)
}

func (g *GlobalAction) LoadCertsAndKeys() error {
	var err error
	g.lClientCert, err = certutil.ParseCertFromPEMFile(g.clientCert)
	if err != nil {
		return fmt.Errorf("configIshareClient: cannot load cert file: %s", err)
	}
	g.lClientKey, err = certutil.ParseRSAPrivateKeyFromPEMFile(g.clientKey)
	if err != nil {
		return fmt.Errorf("configIshareClient: cannot load key file: %s", err)
	}
	if g.clientCACerts != "" {
		g.lClientCACerts, err = certutil.ParseCertsFromFile(g.clientCACerts)
		if err != nil {
			return fmt.Errorf("configIshareClient: cannot load cacerts file: %s", err)
		}
	}
	if g.clientRoots != "" {
		g.lClientRoots, err = certutil.ParseCertPoolFromPEMFile(g.clientRoots)
		if err != nil {
			return fmt.Errorf("configIshareClient: cannot load roots file: %s", err)
		}
	}
	return nil
}

func (g *GlobalAction) SetLogger(env string) error {
	return g.setLogger(env)
}

func (g *GlobalAction) setLogger(env string) error {
	// Create logger.
	log, err := zap.NewProduction()
	if err != nil {
		return err
	}
	// Create dev logger.
	if strings.ToLower(env) == "dev" {
		log, err = zap.NewDevelopment()
		if err != nil {
			return err
		}
	}
	g.logger = zapr.NewLogger(log)

	if g.cliDebug {
		g.logger.Info(g.String())
	}
	return nil
}

func (g *GlobalAction) Logger() logr.Logger {
	if g.logger.GetSink() == nil {
		g.setLogger("dev")
	}
	return g.logger
}

func (g *GlobalAction) LogStartupKV() []interface{} {
	return []interface{}{
		"client-cert", g.clientCert,
		"client-key", g.clientKey,
		"client-cacerts", g.clientCACerts,
		"client-roots", g.clientRoots,
		"client-eori", g.clientEORI,
	}
}
