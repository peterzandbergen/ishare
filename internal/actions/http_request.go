package actions

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"
	"gitlab.com/peterzandbergen/ishare/pkg/proxy"
	"gitlab.com/peterzandbergen/ishare/pkg/td"
	"gitlab.com/peterzandbergen/ishare/pkg/token"

	"github.com/urfave/cli/v2"
	"golang.org/x/oauth2"
)

type HTTPRequestAction struct {
	*GlobalAction

	serverEori       string
	tokenURL         string
	serviceURL       string
	unsafe           bool
	dumpBody         bool
	dumpTokenRequest bool
	dumpRequest      bool
	contentType      string
}

func (a *HTTPRequestAction) Flags() []cli.Flag {
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:        "server-eori",
			Destination: &a.serverEori,
			Required:    true,
			Usage:       "ishare server eori",
			EnvVars:     []string{"ISHARE_SERVER_EORI"},
		},
		&cli.StringFlag{
			Name:        "token-url",
			Destination: &a.tokenURL,
			Required:    true,
			Usage:       "token endpoint url",
			EnvVars:     []string{"ISHARE_TOKEN_URL"},
		},
		&cli.StringFlag{
			Name:        "service-url",
			Destination: &a.serviceURL,
			Required:    true,
			Usage:       "service entpoint url",
			EnvVars:     []string{"ISHARE_SERVER_URL"},
		},
		&cli.BoolFlag{
			Name:        "unsafe",
			Destination: &a.unsafe,
			Required:    false,
			Usage:       "disable jwt validity check",
		},
		&cli.BoolFlag{
			Name:        "dump-body",
			Destination: &a.dumpBody,
			Required:    false,
			Usage:       "dump response body",
		},
		&cli.BoolFlag{
			Name:        "dump-token-request",
			Destination: &a.dumpTokenRequest,
			Required:    false,
			Usage:       "dump outgoing token request",
		},
		&cli.BoolFlag{
			Name:        "dump-request",
			Destination: &a.dumpRequest,
			Required:    false,
			Usage:       "dump outgoing request",
		},
		&cli.StringFlag{
			Name:        "content-type",
			Destination: &a.contentType,
			Value:       "application/json",
			Usage:       "content-type of data, defaults to application/json",
		},
	}
	return append(flags, a.GlobalAction.Flags()...)
}

func (a *HTTPRequestAction) newIshareConfig() (*isharecredentials.Config, error) {
	// Load the certs and key.
	cfg := &isharecredentials.Config{
		TokenURL:      a.tokenURL,
		ClientEORI:    a.clientEORI,
		Cert:          a.lClientCert,
		ServerEORI:    a.serverEori,
		Key:           a.lClientKey,
		CACerts:       a.lClientCACerts,
		RootCerts:     a.lClientRoots,
		TokenBackdate: a.backdate,
	}
	return cfg, nil
}

func (a *HTTPRequestAction) getClaims(cfg *isharecredentials.Config, b []byte) (interface{}, error) {
	tk, err := cfg.ClientToken()
	if err != nil {
		return nil, err
	}
	var obj interface{}
	err = json.Unmarshal(b, &obj)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling data: %w", err)
	}

	// Try to get a token.
	tr, err := token.IsTokenResponse(obj)
	if err != nil {
		// Not a token, return the object.
		return obj, nil
	}
	return tk.GetTokenClaimsOpt(tr, a.unsafe)
}

func (a *HTTPRequestAction) DoRequest(req *http.Request) error {
	cfg, err := a.newIshareConfig()
	if err != nil {
		return fmt.Errorf("new ishare config failed: %w", err)
	}

	// Create token request context.
	ctx, cancel := context.WithTimeout(context.Background(), 300*time.Second)
	defer cancel()
	if a.dumpTokenRequest {
		// get client.
		client := &http.Client{}
		td.InjectLogger(client, a.logger)
		ctx = context.WithValue(ctx, oauth2.HTTPClient, client)
	}

	// Create service request context.
	dumpCtx, dumpCancel := context.WithTimeout(context.Background(), 300*time.Second)
	defer dumpCancel()
	// Inject dumper.
	if a.dumpRequest {
		// get client.
		client := &http.Client{}
		td.InjectLogger(client, a.logger)
		dumpCtx = context.WithValue(ctx, oauth2.HTTPClient, client)
	}
	ts := cfg.TokenSource(ctx)
	hc := oauth2.NewClient(dumpCtx, ts)

	// Set content type if not already set.
	if req.Header.Get("Content-Type") == "" {
		req.Header.Set("Content-Type", a.contentType)
	}

	// Do
	resp, err := hc.Do(req)
	if err != nil {
		return fmt.Errorf("http client do failed for url %s: %w", req.URL.String(), err)
	}

	// Log response info on gzip.
	if a.cliDebug {
		var msg string = "response body was NOT decompressed"
		if resp.Uncompressed {
			msg = "response was decompressed"
		}
		a.logger.Info(msg, proxy.HeadersKV(resp.Header)...)
	}

	// Get the body if any.
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("error reading response body: %w", err)
	}
	// Check the status code.
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		bs := string(b)
		a.logger.Info("response error", "statusCode", resp.StatusCode, "body", bs)
		return fmt.Errorf("error retrieving %s: code=%d, message=%s", a.serviceURL, resp.StatusCode, resp.Status)
	}
	claims, err := a.getClaims(cfg, b)
	if err != nil {
		return fmt.Errorf("error getting claims: %w", err)
	}
	jb, err := json.MarshalIndent(claims, "", "    ")
	if err != nil {
		return fmt.Errorf("marshalling json failed: %w", err)
	}
	fmt.Printf("%s\n", string(jb))

	if a.dumpBody {
		fmt.Printf("-----BEGIN HTTP BODY-----\n%s\n-----END HTTP BODY-----\n", string(b))
	}
	return nil
}
