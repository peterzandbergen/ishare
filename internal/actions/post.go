package actions

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"

	"github.com/urfave/cli/v2"
)

type PostAction struct {
	*HTTPRequestAction

	data        string
	contentType string

	dataBytes []byte
}

// NewGetCommand create a PostAction command.
//
// PostAction retrieves the data for the given url and sends
// the payload to stdout.
// Parameters are:
// 	- server-eori
//  - server uri
//  - service endpoint, full uri
//  - data
func NewPostCommand(global *GlobalAction) *cli.Command {
	cfg := PostAction{
		HTTPRequestAction: &HTTPRequestAction{
			GlobalAction: global,
		},
	}
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:        "data",
			Destination: &cfg.data,
			Required:    true,
			Usage:       "data to post, use @file to post the content of a file",
		},
	}
	cmd := &cli.Command{
		Name:        "post",
		Description: "Posts data to the given service endpoint and retrieves the result.",
		UsageText:   "ishare post [command options]",
		ArgsUsage:   "",
		Usage:       "do POST request",
		Action:      cfg.Action,
		Flags:       append(cfg.HTTPRequestAction.Flags(), flags...),
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd
}

func (a *PostAction) getDataBytes() error {
	if len(a.data) == 0 {
		return errors.New("data is empty")
	}
	if a.data[0] != '@' {
		a.dataBytes = []byte(a.data)
		return nil
	}
	var err error
	a.dataBytes, err = ioutil.ReadFile(a.data[1:])
	if err != nil {
		return fmt.Errorf("failed to read data file: %w", err)
	}
	return nil
}

func (a *PostAction) Action(appCtx *cli.Context) error {
	if err := a.HTTPRequestAction.Action(appCtx); err != nil {
		return fmt.Errorf("global action failed: %w", err)
	}
	
	// Get the data in dataBytes.
	if err := a.getDataBytes(); err != nil {
		return fmt.Errorf("get data bytes failed: %w", err)
	}
	// Create request.
	req, err := http.NewRequest(http.MethodPost, a.serviceURL, bytes.NewBuffer(a.dataBytes))
	if err != nil {
		return fmt.Errorf("error creating POST request for %s: %w", a.serviceURL,  err)
	}
	req.Header.Set("Content-Type", a.contentType)

	return a.DoRequest(req)
}
