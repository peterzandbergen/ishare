package actions

import (
	"context"
	"testing"
)

func TestPost(t *testing.T) {

	args := []string{"isharecli", "post",
		"--client-eori", cbsEori,
		"--client-cert", certCBS,
		"--client-key", keyCBS,
		"--client-cacerts", caCertChain,
		"--client-roots", rootCert,
		"--token-url", schemeOwnerTokenURI,
		"--server-eori", schemeOwnerEori,
		"--data", `{"tripStartDate": "2020-09-29","tripEndDate": "2020-09-30"}`,
		"--service-url", "https://bctn.modalityservices.nl:50333/cbs/data",
		"--dump-request",
		"--log-env", "dev",
	}

	err := NewApp().RunContext(context.Background(), args)
	if err != nil {
		t.Errorf("error: %s", err)
	}
}

func TestPostModality(t *testing.T) {

	args := []string{"isharecli", "post",
		"--client-eori", cbsEori,
		"--client-cert", certCBS,
		"--client-key", keyCBS,
		"--client-cacerts", caCertChain,
		"--client-roots", rootCert,
		"--token-url", "https://bctn-ishare.modality.nl/oauth2.0/token",
		"--server-eori", "EU.EORI.NL000000000",
		"--data", `{"tripStartDate": "2020-09-29","tripEndDate": "2020-09-30", "page": 1}`,
		"--service-url", "https://bctn-ishare.modality.nl/cbs/data",
		// "--dump-request",
		"--log-env", "dev",
	}

	err := NewApp().RunContext(context.Background(), args)
	if err != nil {
		t.Errorf("error: %s", err)
	}
}
