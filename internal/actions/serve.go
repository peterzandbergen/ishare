package actions

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sort"
	"syscall"
	"time"

	"github.com/go-logr/logr"
	"github.com/urfave/cli/v2"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
	"gitlab.com/peterzandbergen/ishare/pkg/ishare"
	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"
	"gitlab.com/peterzandbergen/ishare/pkg/proxy"
)

// ServeAction is filled by cli.
//
// It allows for easy testing.
type ServeAction struct {
	*GlobalAction

	adapterCert   string
	adapterKey    string
	adapterApiKey string

	listenAddress string
	port          int
}

func NewServeCommand(global *GlobalAction) *cli.Command {
	a := ServeAction{
		GlobalAction: global,
	}
	serveFlags := []cli.Flag{
		&cli.StringFlag{
			Name:        "listen-address",
			Destination: &a.listenAddress,
			Value:       "0.0.0.0:8080",
			Usage:       "ishare adapter api key",
			EnvVars:     []string{"ISHARE_ADAPTER_LISTEN_ADDRESS"},
		},
		&cli.IntFlag{
			Name:        "port",
			Destination: &a.port,
			Value:       8080,
			Usage:       "listen port",
			EnvVars:     []string{"ISHARE_ADAPTER_PORT", "PORT", "port"},
		},
		&cli.StringFlag{
			Name:        "adapter-cert",
			Destination: &a.adapterCert,
			// Value:   ".",
			Usage:   "ishare adapter TLS cert",
			EnvVars: []string{"ISHARE_ADAPTER_CERT"},
		},
		&cli.StringFlag{
			Name:        "adapter-key",
			Destination: &a.adapterKey,
			// Value:   ".",
			Usage:   "ishare adapter TLS key",
			EnvVars: []string{"ISHARE_ADAPTER_KEY"},
		},
		&cli.StringFlag{
			Name:        "adapter-api-key",
			Destination: &a.adapterApiKey,
			// Value:   ".",
			Usage:   "ishare adapter api key",
			EnvVars: []string{"ISHARE_ADAPTER_API_KEY"},
		},
	}
	cmd := &cli.Command{
		// Before:      a.Before,
		Name:        "serve",
		Usage:       "start an iShare proxy server",
		Action:      a.Action,
		Description: "proxy server for iShare compliant services, see http://github for more info",
		Flags:       append(serveFlags, a.GlobalAction.Flags()...),
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd
}

func (s *ServeAction) Before(ctx *cli.Context) error {
	if ctx.IsSet("listen-address") && ctx.IsSet("port") {
		s.logger.Info("listen-address and port both set, using listen-address")
	}
	if ctx.IsSet("listen-address") {
		// use the listen address
		return nil
	}
	if ctx.IsSet("port") {
		s.listenAddress = fmt.Sprintf(":%d", s.port)
		return nil
	}
	return nil
}

// configIshareClient initializes and returns an ishare.Config.
//
// The parameters configure the client.
func configIshareClient(eori, certFile, keyFile, cacertsFile, rootsFile string) (*ishare.Config, error) {
	if eori == "" {
		return nil, fmt.Errorf("configIshareClient: eori is empty")
	}
	cert, err := certutil.ParseCertFromPEMFile(certFile)
	if err != nil {
		return nil, fmt.Errorf("configIshareClient: cannot load cert file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyFile)
	if err != nil {
		return nil, fmt.Errorf("configIshareClient: cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(cacertsFile)
	if err != nil {
		return nil, fmt.Errorf("configIshareClient: cannot load cacerts file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootsFile)
	if err != nil {
		return nil, fmt.Errorf("configIshareClient: cannot load roots file: %s", err)
	}
	return &ishare.Config{
		ClientEORI: eori,
		Cert:       cert,
		Key:        key,
		CACerts:    cas,
		Roots:      root,
	}, nil
}

// listenAndServe starts the server in non secure or TLS mode depending.
//
// The server starts in non TLS mode if both certFile and keyFile are empty.
// If either one is not empty, then the server will start in TLS mode using
// the certFile and keyFile.
func listenAndServe(server *http.Server, certFile, keyFile string) error {
	if certFile == "" && keyFile == "" {
		return server.ListenAndServe()
	}
	return server.ListenAndServeTLS(certFile, keyFile)
}

func (a *ServeAction) Action(appCtx *cli.Context) error {
	if err := a.GlobalAction.Action(appCtx); err != nil {
		return err
	}

	if err := a.Before(appCtx); err != nil {
		return fmt.Errorf("before failed: %w", err)
	}

	// Log the settings.
	a.logger.Info("serve cli options", a.LogStartupKV()...)

	// Load the certs.
	if err := a.LoadCertsAndKeys(); err != nil {
		return err
	}

	if a.dryRun {
		return nil
	}
	return a.serve()
}

// serve starts the proxy in blocking mode.
func (a *ServeAction) serve() error {
	cfg := &isharecredentials.Config{
		ClientEORI: a.clientEORI,
		Cert:       a.lClientCert,
		Key:        a.lClientKey,
		CACerts:    a.lClientCACerts,
		RootCerts:  a.lClientRoots,
		TokenBackdate: a.backdate,
	}

	// Create the proxy server.
	prx := proxy.New(cfg, a.adapterApiKey, a.logger)
	// Create the http server with the proxy as handler.
	server := &http.Server{
		Addr:    a.listenAddress,
		Handler: prx,
	}

	// Run the server.
	return a.runServer(server, a.logger)
}

func (a *ServeAction) runServer(server *http.Server, slog logr.Logger) error {
	// Channel for catching the server result.
	done := make(chan error)
	// Start the server in the background.
	go func() {
		a.logger.Info("starting server", "listen_address", server.Addr)
		done <- listenAndServe(server, a.adapterCert, a.adapterKey)
	}()

	// Wait for server to end or ctrl-C
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM)

	// serverDown signals if the server is already down.
	serverDown := false
	var serverErr error
	select {
	case serverErr = <-done:
		slog.Info("server stopped", "err", serverErr.Error())
		serverDown = true
	case s := <-signals:
		slog.Info("signal caught", "signal", s.String())
	}
	if serverDown {
		return serverErr
	}

	// Create time out context
	ctxTime, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	slog.Info("shutting server down")
	err := server.Shutdown(ctxTime)
	if err != nil {
		slog.Error(err, "error shutting server down")
		return err
	}
	slog.Info("server shut down succesfully")
	return nil
}

func (a *ServeAction) String() string {
	return fmt.Sprintf("logEnv=%s, listenAddress=%s, %s", a.logEnv, a.listenAddress, a.GlobalAction.String())
}

func (a *ServeAction) LoadCertsAndKeys() error {
	var err error
	// The things we have to code for a proper error message.
	if a.adapterCert == "" && a.adapterKey != "" {
		return fmt.Errorf("adapter-cert option is missing")
	}
	if a.adapterKey == "" && a.adapterCert != "" {
		return fmt.Errorf("adapter-key option is missing")
	}
	if a.adapterCert != "" {
		a.lAdapterCert, err = certutil.ParseCertFromPEMFile(a.adapterCert)
		if err != nil {
			return fmt.Errorf("configIshareClient: cannot load adapter cert file: %s", err)
		}
	}
	if a.adapterKey != "" {
		a.lAdapterKey, err = certutil.ParseRSAPrivateKeyFromPEMFile(a.adapterKey)
		if err != nil {
			return fmt.Errorf("configIshareClient: cannot adapter key file: %s", err)
		}
	}
	return nil
}

func (a *ServeAction) LogStartupKV() []interface{} {
	return append(a.GlobalAction.LogStartupKV(),
		"listen-address", a.listenAddress,
		"port", a.port,
		"adapter-cert", a.adapterCert,
		"adapter-key", a.adapterKey,
		"adapter-api-key", a.adapterApiKey,
	)
}
