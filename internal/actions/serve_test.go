package actions

import (
	"context"
	"io"
	"net/http"
	"strings"
	"testing"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/proxy"
)

func TestServeDryRun(t *testing.T) {
	args := []string{"isharecli", "serve",
		"--client-eori", cbsEori,
		"--client-cert", certCBS,
		"--client-key", keyCBS,
		"--client-cacerts", caCertChain,
		"--client-roots", rootCert,
		"--port", "30000",
		// "--dry-run",
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go func() {
		err := NewApp().RunContext(ctx, args)
		if err != nil {
			t.Errorf("error: %s", err)
		}
	}()
	time.Sleep(5 * time.Second)
	req, err := http.NewRequest(http.MethodGet, "http://localhost:30000", nil)
	// req.Header.Set(HeaderServerEORI, bananaCoEori) // Bad eori
	req.Header.Set(proxy.HeaderServerEORI, awesomeWidgetsEori)
	req.Header.Set(proxy.HeaderServiceURL, awesomeWidgetsHost+"/me")
	req.Header.Set(proxy.HeaderTokenURL, awesomeWidgetsTokenUri)
	if err != nil {
		t.Fatalf("error creating request: %s", err)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Do error: %s", err)
	}
	msg := strings.Builder{}
	io.Copy(&msg, resp.Body)
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		t.Fatalf("status code error: %d, %s, message: %s", resp.StatusCode, http.StatusText(resp.StatusCode), msg.String())
	}
	cancel()
	t.Errorf("success: body: %s", msg.String())
}

func TestServeModality(t *testing.T) {
	args := []string{"isharecli", "serve",
		"--client-eori", cbsEori,
		"--client-cert", certCBS,
		"--client-key", keyCBS,
		"--client-cacerts", caCertChain,
		"--client-roots", rootCert,
		"--port", "30000",
		// "--dry-run",
		// "--token-url", "https://bctn-ishare.modality.nl/oauth2.0/token",
		// "--server-eori", "EU.EORI.NL000000000",
		// "--data", `{"tripStartDate": "2020-09-29","tripEndDate": "2020-09-30"}`,
		// "--service-url", "https://bctn-ishare.modality.nl/cbs/data",
		// // "--dump-request",
		"--log-env", "dev",
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go func() {
		err := NewApp().RunContext(ctx, args)
		if err != nil {
			t.Errorf("error: %s", err)
		}
	}()
	time.Sleep(5 * time.Second)
	req, err := http.NewRequest(http.MethodPost, "http://localhost:30000", strings.NewReader(`{"tripStartDate":"2020-09-29","tripEndDate":"2020-09-30","page": 1}`))
	// req.Header.Set(HeaderServerEORI, bananaCoEori) // Bad eori
	req.Header.Set(proxy.HeaderServerEORI, "EU.EORI.NL000000000")
	req.Header.Set(proxy.HeaderServiceURL, "https://bctn-ishare.modality.nl/cbs/data")
	req.Header.Set(proxy.HeaderTokenURL, "https://bctn-ishare.modality.nl/oauth2.0/token")
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatalf("error creating request: %s", err)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Do error: %s", err)
	}
	msg := strings.Builder{}
	io.Copy(&msg, resp.Body)
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		t.Fatalf("status code error: %d, %s, message: %s", resp.StatusCode, http.StatusText(resp.StatusCode), msg.String())
	}
	cancel()
	t.Errorf("success: body: %s", msg.String())
}
