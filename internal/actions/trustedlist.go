package actions

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"time"

	"github.com/urfave/cli/v2"

	"gitlab.com/peterzandbergen/ishare/pkg/ishare"
)

type TrustedListAction struct {
	*GlobalAction

	serverEori string
	serverHost string
}

func NewTrustedListCommand(global *GlobalAction) *cli.Command {
	cfg := TrustedListAction{
		GlobalAction: global,
	}
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:        "server-eori",
			Destination: &cfg.serverEori,
			Required:    true,
			Usage:       "ishare server eori",
		},
		&cli.StringFlag{
			Name:        "server-host",
			Destination: &cfg.serverHost,
			Required:    true,
			Usage:       "token endpoint url",
		},
	}
	cmd := &cli.Command{
		Name:        "trusted-list",
		Description: "Retrieves trusted list from the server host.",
		UsageText:   "ishare trusted-list [command options]",
		ArgsUsage:   "",
		Usage:       "retrieve trusted list",
		Action:      cfg.Action,
		Flags:       append(cfg.GlobalAction.Flags(), flags...),
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd
}

func (a *TrustedListAction) Action(appCtx *cli.Context) error {
	if err := a.GlobalAction.Action(appCtx); err != nil {
		return err
	}

	cfg := ishare.Config{
		ClientEORI: a.clientEORI,
		Cert:       a.lClientCert,
		Key:        a.lClientKey,
		CACerts:    a.lClientCACerts,
		Roots:      a.lClientRoots,
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	client, err := cfg.NewClient(ctx, ctx, a.serverHost, a.serverEori)
	if err != nil {
		return fmt.Errorf("error creating client from ishare config: %w", err)
	}
	if client.TrustedListClient == nil {
		return fmt.Errorf("error: server %s does not support trusted list service", a.serverHost)
	}
	list, err := client.TrustedListClient.Get(ctx)
	if err != nil {
		return fmt.Errorf("error retrieving trusted list: %w", err)
	}
	b, err := json.MarshalIndent(list, "", "    ")
	if err != nil {
		return fmt.Errorf("error marshaling trusted list: %w", err)
	}
	fmt.Printf("Trusted list:\n%s\n", string(b))
	return nil
}
