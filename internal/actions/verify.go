package actions

import (
	"errors"
	"sort"

	"github.com/urfave/cli/v2"
)

type VerifyAction struct {
	*GlobalAction
}

func (a *VerifyAction) Action(appCtx *cli.Context) error {
	if err := a.GlobalAction.Action(appCtx); err != nil {
		a.logger.Error(err, "GlobalAction.Action failed")
	}

	if a.dryRun {
		return nil
	}
	return a.verify()
}

// verify checks if all parameters are correct.
//
// It checks
//	- if cert and key point to
func (a *VerifyAction) verify() error {
	return errors.New("verify not implemented yet")
}

func NewVerifyCommand(global *GlobalAction) *cli.Command {
	cfg := VerifyAction{
		GlobalAction: global,
	}
	cmd := &cli.Command{
		Name:   "verify",
		Action: cfg.Action,
		Flags: cfg.GlobalAction.Flags(),
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd
}
