package actions

import (
	"fmt"
	"sort"

	"github.com/urfave/cli/v2"
)

var (
	programVersion string
)

type VersionAction struct {
}

func NewVersionCommand() *cli.Command {
	a := &VersionAction{}
	cmd := &cli.Command{
		Name:        "version",
		Description: "Show the program version.",
		UsageText:   "ishare version",
		ArgsUsage:   "",
		Usage:       "do POST request",
		Action:      a.Action,
	}
	sort.Sort(cli.FlagsByName(cmd.Flags))
	return cmd

}

func (a *VersionAction) Action(ctx *cli.Context) error {
	fmt.Println(programVersion)
	return nil
}
