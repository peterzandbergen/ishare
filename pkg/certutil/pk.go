package certutil

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
)

// ParseRSAPrivateKeyFromPEM parses PEM encoded PKCS1 or PKCS8 private key
func ParseRSAPrivateKeyFromPEM(key []byte) (*rsa.PrivateKey, error) {
	var err error

	// Parse PEM block
	var block *pem.Block
	if block, _ = pem.Decode(key); block == nil {
		return nil, ErrKeyMustBePEMEncoded
	}

	var parsedKey interface{}
	if parsedKey, err = x509.ParsePKCS1PrivateKey(block.Bytes); err != nil {
		if parsedKey, err = x509.ParsePKCS8PrivateKey(block.Bytes); err != nil {
			return nil, err
		}
	}

	var pkey *rsa.PrivateKey
	var ok bool
	if pkey, ok = parsedKey.(*rsa.PrivateKey); !ok {
		return nil, ErrKeyIsNotRSA
	}

	return pkey, nil
}

// ParseRSAPrivateKeyFromPEMFile parses the RSA private key from a pem formatted file.
func ParseRSAPrivateKeyFromPEMFile(filename string) (*rsa.PrivateKey, error) {
	pkb, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	k, err := ParseRSAPrivateKeyFromPEM(pkb)
	if err != nil {
		return nil, err
	}
	return k, nil
}
