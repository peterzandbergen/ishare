package certutil

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io/ioutil"
)

var (
	ErrNoCertsFound = errors.New("no certs found")
	ErrNoCertFound  = errors.New("no cert found")
	ErrKeyMustBePEMEncoded  = errors.New("key must be pem encoded")
	ErrKeyIsNotRSA          = errors.New("key is not of type RSA")
)

// ParseCertFromPEMFile reads a certificate from a pem encoded file.
func ParseCertFromPEMFile(file string) (*x509.Certificate, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	p, _ := pem.Decode(data)
	if p.Type != "CERTIFICATE" {
		return nil, ErrNoCertFound
	}
	cert, err := x509.ParseCertificate(p.Bytes)
	if err != nil {
		return nil, err
	}
	return cert, nil
}

func AddCerts(pool *x509.CertPool, certs []*x509.Certificate) {
	for _, c := range certs {
		pool.AddCert(c)
	}
}

func PoolFromCerts(certs []*x509.Certificate) *x509.CertPool {
	pool := x509.NewCertPool()
	AddCerts(pool, certs)
	return pool
}

func ParseCertsFromFile(file string) ([]*x509.Certificate, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	var res []*x509.Certificate
	for p, rest := pem.Decode(data); p != nil; p, rest = pem.Decode(rest) {
		// Skip non certs
		if p.Type != "CERTIFICATE" {
			continue
		}
		cert, err := x509.ParseCertificate(p.Bytes)
		if err != nil {
			return nil, err
		}
		res = append(res, cert)
	}
	return res, nil
}

// ParseCertPoolFromPEMFile reads the pem encoded certificates from a file into a cert pool.
func ParseCertPoolFromPEMFile(file string) (*x509.CertPool, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	pool := x509.NewCertPool()
	if !pool.AppendCertsFromPEM(data) {
		return nil, ErrNoCertsFound
	}
	return pool, nil
}

// RawCerts returns the Raw []byte representation of the certificates in an array.
func RawCerts(certs []*x509.Certificate) [][]byte {
	var res [][]byte
	for _, c := range certs {
		res = append(res, c.Raw)
	}
	return res
}
