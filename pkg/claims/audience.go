package claims

import (
	"encoding/json"
	"fmt"
)

type Audience []string

// UnmarshalJSON reads an audience from its JSON representation.
func (s *Audience) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}

	switch v := v.(type) {
	case string:
		*s = []string{v}
	case []interface{}:
		a := make([]string, len(v))
		for i, e := range v {
			s, ok := e.(string)
			if !ok {
				return fmt.Errorf("error unmarshalling audience")
			}
			a[i] = s
		}
		*s = a
	default:
		return fmt.Errorf("error unmarshalling audience")
	}

	return nil
}

func (s Audience) Contains(v string) bool {
	for _, a := range s {
		if a == v {
			return true
		}
	}
	return false
}
