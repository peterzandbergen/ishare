package claims

// Claims represents public claim values (as specified in RFC 7519).
type Claims struct {
	Issuer    string       `json:"iss,omitempty"`
	Subject   string       `json:"sub,omitempty"`
	Audience  Audience     `json:"aud,omitempty"`
	Expiry    *NumericDate `json:"exp,omitempty"`
	NotBefore *NumericDate `json:"nbf,omitempty"`
	IssuedAt  *NumericDate `json:"iat,omitempty"`
	ID        string       `json:"jti,omitempty"`
}

func (c *Claims) GetClaims() Claims {
	return *c
}

// ClaimsGetter contains a method to get the jwt.Claims.
type ClaimsGetter interface {
	// GetClaims returns the jwt.Claims.
	GetClaims() Claims
}
