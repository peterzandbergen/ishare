package claims

// type GenericClaims map[string]interface{}

// func (f GenericClaims) GetClaims() Claims {
// 	return Claims{}
// }

// func IsTokenResponse(any interface{}) (tokenString string, err error) {
// 	// Cast to map.
// 	obj, ok := any.(map[string]interface{})
// 	if !ok {
// 		return "", fmt.Errorf("any is not an object, may be an array")
// 	}

// 	if l := len(obj); l != 1 {
// 		// Not a token, return the complete response.
// 		return "", fmt.Errorf("expected one property, found %d", len(obj))
// 	}
// 	// One property found. Check if it could be a token.
// 	for k := range obj {
// 		v, ok := obj[k].(string)
// 		if !ok {
// 			return "", fmt.Errorf("property %s of type %s, expected string", k, reflect.TypeOf(obj[k]))
// 		}
// 		if strings.Contains(strings.ToLower(v), "token") {
// 			return v, nil
// 		}
// 	}
// 	return "", fmt.Errorf("no token found")
// }

// func GetClaims(cfg *isharecredentials.Config, b []byte) (interface{}, error) {
// 	// Unmarshal to any.
// 	var any interface{}
// 	err := json.Unmarshal(b, &any)
// 	if err != nil {
// 		return nil, fmt.Errorf("error unmarshalling token tp any: %w", err)
// 	}

// 	// Try to find a token.
// 	token, err := IsTokenResponse(any)
// 	if err != nil {
// 		return any, nil
// 	}

// 	// Check if the mandatory claims are correct.
// 	if !a.unsafe {
// 		var resp Claims
// 		if err := cfg.UnmarshalJWT(token, &resp); err != nil {
// 			return nil, fmt.Errorf("error unmarshaling claims: %w", err)
// 		}
// 	}
// 	// Get all claims.
// 	var resp GenericClaims
// 	if err := cfg.UnmarshalJWTUnsafe(token, &resp); err != nil {
// 		return nil, fmt.Errorf("error unmarshaling claims: %w", err)
// 	}
// 	return resp, nil
// }
