package dto

import "gitlab.com/peterzandbergen/ishare/pkg/claims"

type DelegationResponse struct {
	DelegationToken string `json:"delegation_token"`
}

func (d DelegationResponse) Token() string {
	return d.DelegationToken
}

type DelegationRequest struct {
	PolicyIssuer   string            `json:"policyIssuer,omitempty"`
	Target         *DelegationTarget `json:"target,omitempty"`
	PolicySets     []PolicySet       `json:"policySets,omitempty"`
	DelegationPath []string          `json:"delegationPath,omitempty"`
	PreviousSteps  []string          `json:"previousSteps,omitempty"`
}

type DelegationTarget struct {
	AccessSubject string `json:"accessSubject,omitempty"`
}

type DelegationEvidenceClaims struct {
	claims.Claims
	DelegationEvidence `json:"delegationEvidence,omitempty"`
}

type DelegationEvidence struct {
	NotBefore    int               `json:"notBefore,omitempty"`
	NotOnOrAfter int               `json:"notOnOrAfter,omitempty"`
	PolicyIssuer string            `json:"policyIssuer,omitempty"`
	Target       *DelegationTarget `json:"target,omitempty"`
	PolicySets   []PolicySet       `json:"policySets,omitempty"`
}

type PolicySet struct {
	MaxDelegationDepth int              `json:"maxDelegationDepth,omitempty"`
	Target             *PolicySetTarget `json:"target,omitempty"`
	Policies           []Policy         `json:"policies,omitempty"`
}

type PolicySetTarget struct {
	Environment *Environment `json:"environment,omitempty"`
}

type Policy struct {
	Target PolicyTarget `json:"target,omitempty"`
	Rules  []Rule       `json:"rules,omitempty"`
}

type PolicyTarget struct {
	Resource    *Resource    `json:"resource,omitempty"`
	Actions     []string     `json:"actions,omitempty"`
	Environment *Environment `json:"environment,omitempty"`
}

type Rule struct {
	Effect string      `json:"effect,omitempty"`
	Target *RuleTarget `json:"target,omitempty"`
}

type RuleTarget struct {
	Resource *Resource `json:"resource,omitempty"`
	Actions  []string  `json:"actions,omitempty"`
}

type Resource struct {
	Type        string   `json:"type,omitempty"`
	Identifiers []string `json:"identifiers,omitempty"`
	Attributes  []string `json:"attributes,omitempty"`
}

type Environment struct {
	ServiceProviders []string `json:"serviceProviders,omitempty"`
	Licenses         []string `json:"licenses,omitempty"`
}
