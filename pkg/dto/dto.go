package dto

import (
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/claims"
)

type SchemeRole struct {
	Role string `json:"role,omitempty"`
}

type SupportedVersion struct {
	Version           string             `json:"version,omitempty"`
	SupportedFeatures []SupportedFeature `json:"supported_features,omitempty"`
}

type SupportedFeature struct {
	Public     []FeatureObject `json:"public,omitempty"`
	Restricted []FeatureObject `json:"restricted,omitempty"`
	Private    []FeatureObject `json:"private,omitempty"`
}

type FeatureObject struct {
	Id            string `json:"id,omitempty"`
	Feature       string `json:"feature,omitempty"`
	Description   string `json:"description,omitempty"`
	Url           string `json:"url,omitempty"`
	TokenEndpoint string `json:"token_endpoint,omitempty"`
}

type CapabilitiesResponse struct {
	CapabilitiesToken string `json:"capabilities_token,omitempty"`
}

func (c CapabilitiesResponse) Token() string {
	return c.CapabilitiesToken
}

type CapabilitiesInfoClaims struct {
	claims.Claims
	CapabilitiesInfo *CapabilitiesInfo `json:"capabilities_info,omitempty"`
}

type CapabilitiesInfo struct {
	PartyId           string             `json:"party_id,omitempty"`
	IshareRoles       []SchemeRole       `json:"ishare_roles,omitempty"`
	SupportedVersions []SupportedVersion `json:"supported_versions,omitempty"`
}

type PartiesResponse struct {
	PartiesToken string `json:"parties_token,omitempty"`
}

func (p PartiesResponse) Token() string {
	return p.PartiesToken
}

type PartiesInfoClaims struct {
	claims.Claims
	PartiesInfo *PartiesInfo `json:"parties_info,omitempty"`
}

type PartiesInfo struct {
	Count int32   `json:"count,omitempty"`
	Data  []Party `json:"data,omitempty"`
}

type Certification struct {
	Role      string    `json:"role,omitempty"`
	StartDate time.Time `json:"start_date,omitempty"`
	EndDate   time.Time `json:"end_date,omitempty"`
	Loa       int32     `json:"loa,omitempty"`
}

type Adherence struct {
	Status    string    `json:"status,omitempty"`
	StartDate time.Time `json:"start_date,omitempty"`
	EndDate   time.Time `json:"end_date,omitempty"`
}

type PartyResponse struct {
	PartyToken string `json:"party_token,omitempty"`
}

func (p PartyResponse) Token() string {
	return p.PartyToken
}

type PartyInfoClaims struct {
	claims.Claims
	PartyInfo *Party `json:"party_info,omitempty"`
}

type Party struct {
	PartyId        string          `json:"party_id,omitempty"`
	PartyName      string          `json:"party_name,omitempty"`
	Adherence      *Adherence      `json:"adherence,omitempty"`
	Certifications []Certification `json:"certifications,omitempty"`
	CapabilityUrl  string          `json:"capability_url,omitempty"`
}

type TrustedListResponse struct {
	TrustedListToken string `json:"trusted_list_token,omitempty"`
}

func (t TrustedListResponse) Token() string {
	return t.TrustedListToken
}

type TrustedListClaims struct {
	claims.Claims
	CertificateAuthorities []CertificateAuthority `json:"trusted_list,omitempty"`
}

type CertificateAuthority struct {
	Subject                string `json:"subject,omitempty"`
	CertificateFingerprint string `json:"certificate_fingerprint,omitempty"`
	Validity               string `json:"validity,omitempty"`
	Status                 string `json:"status,omitempty"`
}
