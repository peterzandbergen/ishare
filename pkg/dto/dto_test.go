package dto

import (
	"encoding/json"
	"testing"
)

const ( 
	modalityResponse = `{
		"iss": "EU.EORI.NL809245383",
		"sub": "EU.EORI.NL809245383",
		"jti": "Us6lbq4OIsV00kCZ_pOV0A",
		"iat": 1633018049,
		"nbf": 1633018049,
		"exp": 1633018079,
		"capabilities_info": {
		  "party_id": "EU.EORI.NL809245383",
		  "ishare_roles": [
			{
			  "role": "Service Provider"
			}
		  ],
		  "supported_versions": [
			{
			  "version": "1.0",
			  "supported_features": [
				  {
				"public": [],
				"restricted": []
			  }
			  ]
			}
		  ]
		}
	  }`

	  modalityResponse1 = `{
		"iss": "EU.EORI.NL809245383",
		"sub": "EU.EORI.NL809245383",
		"jti": "Us6lbq4OIsV00kCZ_pOV0A",
		"iat": 1633018049,
		"nbf": 1633018049,
		"exp": 1633018079,
		"capabilities_info": {
		  "party_id": "EU.EORI.NL809245383",
		  "ishare_roles": [
			  {
				"role": "Service Provider"
			  }
			]
		}
	  }`

	  modalityResponse2 = `{
		"iss": "EU.EORI.NL809245383",
		"sub": "EU.EORI.NL809245383",
		"jti": "Us6lbq4OIsV00kCZ_pOV0A",
		"iat": 1633018049,
		"nbf": 1633018049,
		"exp": 1633018079,
		"capabilities_info": {
		  "party_id": "EU.EORI.NL809245383"
		},
		"ishare_roles": [
			{
			  "role": "Service Provider"
			}
		  ],
		  "supported_versions": [
			{
			  "version": "1.0",
			  "supported_features": {
				"public": [],
				"restricted": []
			  }
			}
		  ]
	  }`

)


func TestModalityResponse(t *testing.T) {
	c := CapabilitiesInfoClaims{}
	err := json.Unmarshal([]byte(modalityResponse), &c)
	if err != nil {
		t.Errorf("error unmashaling capability claims: %s", err)
	}
	err = json.Unmarshal([]byte(modalityResponse1), &c)
	if err != nil {
		t.Errorf("error unmashaling capability claims: %s", err)
	}
	err = json.Unmarshal([]byte(modalityResponse2), &c)
	if err != nil {
		t.Errorf("error unmashaling capability claims: %s", err)
	}
}

func TestClaimGetters(t *testing.T) {
	// var _ ClaimsGetter = &JWTClaims{}
	// var _ ClaimsGetter = CapabilitiesInfoClaims{}
	// var _ ClaimsGetter = TrustedListClaims{}
}
