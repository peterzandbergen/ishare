package ishare

// AccessTokenService retrieves an access token.
// It is provided for convenience but is not required for using the package.
type AccessTokenService struct {
	BaseService
}
