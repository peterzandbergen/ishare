package ishare

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"

	"gitlab.com/peterzandbergen/ishare/pkg/claims"
)

type UnmarshalJWTFunc func(token string, clms claims.ClaimsGetter) error

type TokenResponse interface {
	Token() string
}

type BaseService struct {
	ErrPrefix    string
	uri          string
	hc           *http.Client
	unmarshalJWT UnmarshalJWTFunc
}

func (b BaseService) get(ctx context.Context,
	uri string,
	values map[string]string,
	body io.ReadCloser,
	response interface{},
	claims claims.ClaimsGetter) error {

	// Create request
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, uri, nil)
	if err != nil {
		return err
	}

	// Set default headers.
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json")

	// Add the parameters
	if values != nil {
		vu := url.Values{}
		for k, v := range values {
			vu.Add(k, v)
		}
		req.URL.RawQuery = vu.Encode()
	}
	// Add the body
	req.Body = body

	// Do request.
	rsp, err := b.hc.Do(req)

	// Handle the error code.
	if err := httpError(uri, "client.Do", err, rsp); err != nil {
		return err
	}

	// Unmarshal the body to the response
	if err := b.unmarshalBody(rsp.Body, response); err != nil {
		return err
	}

	// Get the claims
	if claims != nil {
		return b.claims(response, claims)
	}
	return nil
}

func (b BaseService) unmarshalBody(r io.Reader, obj interface{}) error {
	d, err := io.ReadAll(r)
	if err != nil {
		return err
	}
	// Unmarshal response.
	json.Unmarshal(d, obj)
	if err != nil {
		return err
	}
	return nil
}

func (b BaseService) claims(resp interface{}, claims claims.ClaimsGetter) error {
	var tr TokenResponse
	tr, ok := resp.(TokenResponse)
	if !ok {
		return errors.New("no token found")
	}
	return b.unmarshalJWT(tr.Token(), claims)
}
