package ishare

import (
	"context"
	"fmt"

	"gitlab.com/peterzandbergen/ishare/pkg/dto"
)

type CapabilitiesService struct {
	BaseService
}

type CapabilitiesInfo = dto.CapabilitiesInfo

func (c *CapabilitiesService) Get(ctx context.Context) (*CapabilitiesInfo, error) {
	var response dto.CapabilitiesResponse
	var claims dto.CapabilitiesInfoClaims

	if err := c.get(ctx, c.uri, nil, nil, &response, &claims); err != nil {
		return nil, fmt.Errorf("Error getting capabilitiies from %s: %w", c.uri, err)
	}

	return claims.CapabilitiesInfo, nil
}
