package ishare

import (
	"context"
	"crypto/rsa"
	"crypto/x509"
	"fmt"
	"net/http"
	"strings"

	"golang.org/x/oauth2"

	"gitlab.com/peterzandbergen/ishare/pkg/dto"
	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"
)

// ErrInvalidConfig is returned if the config does not contain the required info to create a new client.
type ErrInvalidConfig string

func (e ErrInvalidConfig) Error() string {
	return string(e)
}

type CertificateValidationService struct{}

type GenerateClientAssertionService struct{}

type VersionsInformationService struct{}

type AuthorizeService struct{}

// Config contains the required information to create iShare.Client.
//
// All fields must be specified, if not the NewClient method will fail.
type Config struct {
	// ClientEORI must contain the EORI identifier of the client party.
	ClientEORI string
	// Cert is the certificate that identifies the client.
	Cert *x509.Certificate
	// Key is the private key that the client uses to sign the JWT tokens with.
	Key *rsa.PrivateKey
	// CACerts contains the intermediate certificates between the client cert and one of certificates in Roots.
	CACerts []*x509.Certificate
	// Roots contains the root certificates that are used to verify the received JWT tokens.
	Roots *x509.CertPool
}

func (c *Config) isValid() error {
	res := strings.Builder{}

	add := func(s string) {
		if res.Len() > 0 {
			res.WriteString(", ")
		}
		res.WriteString(s)
	}
	if c.ClientEORI == "" {
		add("ClientEORI is not specified")
	}
	if c.Cert == nil {
		add("Cert is not specified")
	}
	if c.Key == nil {
		add("Key is not specified")
	}
	if c.CACerts == nil {
		add("CACerts is not specified")
	}
	if res.Len() > 0 {
		return ErrInvalidConfig(res.String())
	}
	return nil
}

// NewClient creates a new iShare client from the config values and the server url and server EORI.
//
// If the config is not fully specified, the method will return the ErrInvalidConfig error.
//
// The serverURL should point to the base url of the server. The method contacts the server's
// capabilities enpoint to determine the available services and populates the XxxClients.
//
// The caller should check each sub client in the client for nil to determine if the server
// implements the capability.
func (cfg *Config) NewClient(
	ctx context.Context,
	clientCtx context.Context,
	serverURL string,
	serverEORI string) (*Client, error) {

	if err := cfg.isValid(); err != nil {
		return nil, fmt.Errorf("Config error: %w", err)
	}

	return newClient(ctx, clientCtx,
		serverURL,
		cfg.ClientEORI,
		serverEORI,
		cfg.Cert,
		cfg.Key,
		cfg.CACerts,
		cfg.Roots)
}

// Client represents an iShare service consumer for the communication with one ishare server.
//
// It has a sub client for each possible capability. You need to test if a sub client is nil before
// using it.
type Client struct {
	// CapabilitiesClient exposes the capabilities resource of the server.
	CapabilitiesClient *CapabilitiesService
	// PartiesClient exposes the parties resource.
	PartiesClient *PartiesService
	// PartyClient exposes a party resource.
	PartyClient *PartyService
	// TrustedListClient exposes the trusted list resource.
	TrustedListClient *TrustedListService
	// DelegationClient gives access to the delegation resource of the server. Can be nil.
	DelegationClient              *DelegationService
	AccessTokenClient             *AccessTokenService
	CertificateValidationClient   *CertificateValidationService
	GenerateClientAssertionClient *GenerateClientAssertionService
	VersionsInformationClient     *VersionsInformationService
	AuthorizeClient               *AuthorizeService
	// ExtraClients holds the clients that are specific for this server. This is normally filled for a service producer.
	ExtraClients map[string]*ExtraService

	cfg    *isharecredentials.Config
	client *http.Client
}

func clientFromContext(ctx context.Context) *http.Client {
	if v, ok := ctx.Value(oauth2.HTTPClient).(*http.Client); ok {
		return v
	}
	return &http.Client{}
}

func (c *Client) initCapabilitiesClient(serverURL string) error {
	serverURL = strings.TrimRight(serverURL, "/") + "/capabilities"
	// Init the public capabilities client.
	c.CapabilitiesClient = &CapabilitiesService{
		BaseService{
			hc:           &http.Client{},
			uri:          serverURL,
			unmarshalJWT: c.cfg.UnmarshalJWT,
		},
	}
	return nil
}

// newClient creates a new iShare Client.
//
//   - ctx is the context with a deadline
//   - clientCtx can contain a oauth.HTTPClient value and should stay
//     valid during the lifetime of the Client.
//
// The ctx parameter needs to be passed if you need the client to retrieve the
// capabilities with a modified http.Client.
func newClient(
	ctx context.Context,
	clientCtx context.Context,
	serverURL string,
	clientEORI string,
	serverEORI string,
	cert *x509.Certificate,
	key *rsa.PrivateKey,
	caCerts []*x509.Certificate,
	roots *x509.CertPool) (*Client, error) {

	nc := &Client{
		cfg: &isharecredentials.Config{
			// TokenURL:   tokenURL,
			ClientEORI: clientEORI,
			ServerEORI: serverEORI,
			Cert:       cert,
			Key:        key,
			CACerts:    caCerts,
			RootCerts:  roots,
		},
		ExtraClients: map[string]*ExtraService{},
	}
	if err := nc.initCapabilitiesClient(serverURL); err != nil {
		return nil, err
	}
	// Init with given client.
	nc.client = clientFromContext(clientCtx)
	if err := nc.initCapabilities(ctx); err != nil {
		return nil, err
	}
	// Set the token url.
	nc.cfg.TokenURL = nc.AccessTokenClient.uri
	// Init again with oauth client.
	nc.client = nc.cfg.Client(clientCtx)
	if err := nc.initCapabilities(ctx); err != nil {
		return nil, err
	}
	return nc, nil
}

// initCapabilities initializes the client and populates the client with the
// available sub clients.
// You can pass a custome http client in the context with the oauth2.HTTPClient value.
func (c *Client) initCapabilities(ctx context.Context) error {
	// Get the capabilities.
	caps, err := c.CapabilitiesClient.Get(ctx)
	if err != nil {
		return fmt.Errorf("error getting capabilities: %w", err)
	}
	// Populate the services from the capabilities.
	for _, f := range caps.SupportedVersions[0].SupportedFeatures {
		for _, pf := range f.Public {
			c.addFeature(pf, pf.Url)
		}
	}
	return nil
}

func (c *Client) addFeature(feature dto.FeatureObject, url string) {
	switch feature.Feature {
	case "capabilities":
		// already created
	case "access token":
		c.AccessTokenClient = &AccessTokenService{
			BaseService{
				hc:           &http.Client{},
				uri:          feature.Url,
				unmarshalJWT: c.cfg.UnmarshalJWT,
			},
		}
	case "parties information":
		// Create parties service.
		c.PartiesClient = &PartiesService{
			BaseService{
				hc:           c.client,
				uri:          feature.Url,
				unmarshalJWT: c.cfg.UnmarshalJWT,
			},
		}
	case "party information":
		// Create party service.
		c.PartyClient = &PartyService{
			BaseService{
				hc:           c.client,
				uri:          feature.Url,
				unmarshalJWT: c.cfg.UnmarshalJWT,
			},
		}
	case "trusted_list":
		// Create trusted list service.
		c.TrustedListClient = &TrustedListService{
			BaseService{
				hc:           c.client,
				uri:          feature.Url,
				unmarshalJWT: c.cfg.UnmarshalJWT,
			},
		}
	case "delegation":
		// Create delegation service.
		c.DelegationClient = &DelegationService{
			BaseService{
				hc:           c.client,
				uri:          feature.Url,
				unmarshalJWT: c.cfg.UnmarshalJWT,
			},
		}
	case "certificate validation":
	case "generate client assertion":
	case "version information":
	case "authorize endpoint":

	default:
		c.ExtraClients[feature.Feature] = &ExtraService{
			BaseService: BaseService{
				hc:           c.client,
				uri:          feature.Url,
				unmarshalJWT: c.cfg.UnmarshalJWT,
			},
			Feature: feature,
			c:       c,
		}

	}
}

func httpError(uri, message string, err error, resp *http.Response) error {
	if err != nil {
		return fmt.Errorf("%s: error issuing request to %s: %w", uri, message, err)
	}
	if resp.StatusCode >= 300 {
		return fmt.Errorf("%s: error issuing request to %s: %d %s", uri, message, resp.StatusCode, http.StatusText(resp.StatusCode))
	}
	return nil
}

// HTTPClient returns an http.Client that can be used to send iShare authenticated http requests.
func (c *Client) HTTPClient() *http.Client {
	return c.client
}
