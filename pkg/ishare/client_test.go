package ishare

import (
	"context"
	"testing"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"
)

const (
	cbsEori = "EU.EORI.NL51197073"
	certCBS = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem"
	keyCBS  = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem"

	caCertChain = "/home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem"
	rootCert    = "/home/peza/Documents/iShare/iShareTestKey2/root.pem"

	schemeOwnerHost            = "https://scheme.isharetest.net"
	schemeOwnerTokenURI        = "https://scheme.isharetest.net/connect/token"
	schemeOwnerCapabilitiesURI = "https://scheme.isharetest.net/capabilities"
	schemeOwnerEori            = "EU.EORI.NL000000000"

	warehouse13Host = "https://w13.isharetest.net"
	warehouse13Eori = "EU.EORI.NL000000003"

	authorizationRegistryHost = "https://ar.isharetest.net"
	authorizationRegistryEori = "EU.EORI.NL000000004"

	bananaCoEori     = "EU.EORI.NL000000005"
	bananaCoHost     = "https://banana.isharetest.net"
	bananaCoTokenUri = "https://banana.isharetest.net/connect/token"

	awesomeWidgetsHost = "https://awesome.isharetest.net"
	awesomeWidgetsEori = "EU.EORI.NL000000002"
)

func TestConfigTokenSource(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}

	cfg := &isharecredentials.Config{
		ClientEORI: cbsEori,
		ServerEORI: schemeOwnerEori,
		TokenURL:   schemeOwnerTokenURI,
		Cert:       cert,
		CACerts:    cas,
		RootCerts:  root,
		Key:        key,
	}
	ts := cfg.TokenSource(context.Background())
	_, err = ts.Token()
	if err != nil {
		t.Fatalf("cannot get token from token source: %s", err)
	}

	tk, err := cfg.Token(context.Background())
	if err != nil {
		t.Fatalf("cannot get token: %s", err)
	}
	_ = tk
}

func TestConfigTokenSourceBanana(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}

	cfg := &isharecredentials.Config{
		ClientEORI: cbsEori,
		ServerEORI: bananaCoEori,
		TokenURL:   bananaCoTokenUri,
		Cert:       cert,
		CACerts:    cas,
		RootCerts:  root,
		Key:        key,
	}
	ts := cfg.TokenSource(context.Background())
	_, err = ts.Token()
	if err != nil {
		t.Fatalf("cannot get token from token source: %s", err)
	}

	tk, err := cfg.Token(context.Background())
	if err != nil {
		t.Fatalf("cannot get token: %s", err)
	}
	_ = tk
}

func TestCapabilitiesGet(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		schemeOwnerHost,
		cbsEori,
		schemeOwnerEori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	_ = client

	ci, err := client.CapabilitiesClient.Get(context.Background())
	if err != nil {
		t.Fatalf("error Get: %s", err)
	}
	_ = ci
}

func TestInitClient(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		schemeOwnerHost,
		cbsEori,
		schemeOwnerEori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	_ = client
}

func TestCapabiltiesFromClient(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		schemeOwnerHost,
		cbsEori,
		schemeOwnerEori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	ci, err := client.CapabilitiesClient.Get(context.Background())
	if err != nil {
		t.Fatalf("error get capabilities: %s", err)
	}
	for _, f := range ci.SupportedVersions[0].SupportedFeatures[0].Public {
		t.Logf("feature.Feature %s", f.Feature)
	}
	_ = ci
}

func TestInitClientWarehouse13(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		warehouse13Host,
		cbsEori,
		warehouse13Eori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	_ = client
}

func TestInitAuthReg(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		authorizationRegistryHost,
		cbsEori,
		authorizationRegistryEori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	_ = client
}

func TestEndpoints(t *testing.T) {
	cases := []struct {
		host string
		eori string
	}{
		{host: schemeOwnerHost, eori: schemeOwnerEori},
		{host: warehouse13Host, eori: warehouse13Eori},
		{host: bananaCoHost, eori: bananaCoEori},
		{host: authorizationRegistryHost, eori: authorizationRegistryEori},
		{host: awesomeWidgetsHost, eori: awesomeWidgetsEori},
	}
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	cfg := Config{
		ClientEORI: cbsEori,
		Cert:       cert,
		Key:        key,
		CACerts:    cas,
		Roots:      root,
	}

	for _, c := range cases {
		client, err := cfg.NewClient(ctx, context.Background(), c.host, c.eori)
		if err != nil {
			t.Errorf("error NewClient failed for %s: %s", c.host, err)
		}
		_ = client
	}
}

func TestAppendNilMap(t *testing.T) {
	m := map[string]string{}

	m["test"] = "value"
}
