package ishare

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/peterzandbergen/ishare/pkg/dto"
)

// Check if DelegationResponse implements the Token interface.
var _ TokenResponse = dto.DelegationResponse{}

type DelegationService struct {
	BaseService
}

type DelegationEvidence = dto.DelegationEvidence

func (c *DelegationService) Get(ctx context.Context, delegationRequest *dto.DelegationRequest) (*DelegationEvidence, error) {
	var response dto.DelegationResponse
	var claims dto.DelegationEvidenceClaims

	// Marshal the request.
	b, err := json.Marshal(delegationRequest)
	if err != nil {
		return nil, err
	}
	body := io.NopCloser(bytes.NewBuffer(b))

	if err = c.get(ctx, c.uri, nil, body, &response, &claims); err != nil {
		return nil, fmt.Errorf("Error getting delegation evidence: %w", err)
	}
	return &claims.DelegationEvidence, nil
}
