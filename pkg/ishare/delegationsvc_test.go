package ishare

import (
	"context"
	"testing"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
	"gitlab.com/peterzandbergen/ishare/pkg/dto"
)

const (
	abcTrucking = ""
)

func newTestDelegationRequest() *dto.DelegationRequest {
	return &dto.DelegationRequest{
		PolicyIssuer: bananaCoEori,
		Target: &dto.DelegationTarget{
			AccessSubject: abcTrucking,
		},
		PolicySets: []dto.PolicySet{
			{
				MaxDelegationDepth: 5,
				Target: &dto.PolicySetTarget{
					Environment: &dto.Environment{
						Licenses: []string{
							"bla",
						},
					},
				},
				Policies: []dto.Policy{
					{
						Target: dto.PolicyTarget{
							Resource: &dto.Resource{
								Type: "GS1.CONTAINER",
								Attributes: []string{
									"GS1.CONTAINER.ATTRIBUTE.ETA",
									"GS1.CONTAINER.ATTRIBUTE.WEIGHT",
								},
								Identifiers: []string{
									"180621.CONTAINER-Z",
								},
							},
							Actions: []string{
								"ISHARE.READ",
								"ISHARE.CREATE",
								"ISHARE.UPDATE",
								"ISHARE.DELETE",
							},
							Environment: &dto.Environment{
								ServiceProviders: []string{
									warehouse13Eori,
								},
							},
						},
						Rules: []dto.Rule{
							{
								Effect: "Permit",
							},
						},
					},
				},
			},
		},
	}
}

func newTestDelegationRequestHigh() *dto.DelegationRequest {
	return &dto.DelegationRequest{
		PolicyIssuer: abcTrucking,
		Target: &dto.DelegationTarget{
			AccessSubject: bananaCoEori,
			// AccessSubject: abcTrucking,
		},
		PolicySets: []dto.PolicySet{
			{
				// MaxDelegationDepth: 5,
				// Target: &PolicySetsTarget{
				// 	Environment: &PolicySetsTargetEnvironment{
				// 		Licenses: []string{
				// 			"bla",
				// 		},
				// 	},
				// },
				Policies: []dto.Policy{
					{
						Target: dto.PolicyTarget{
							Resource: &dto.Resource{
								Type: "GS1.CONTAINER",
								Attributes: []string{
									"GS1.CONTAINER.ATTRIBUTE.ETA",
									"GS1.CONTAINER.ATTRIBUTE.WEIGHT",
								},
								Identifiers: []string{
									"180621.CONTAINER-Z",
								},
							},
							Actions: []string{
								"ISHARE.READ",
								// "ISHARE.CREATE",
								// "ISHARE.UPDATE",
								// "ISHARE.DELETE",
							},
							Environment: &dto.Environment{
								ServiceProviders: []string{
									warehouse13Eori,
								},
							},
						},
						Rules: []dto.Rule{
							{
								Effect: "Permit",
							},
						},
					},
				},
			},
		},
	}
}

func TestGetDelegationHigh(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		bananaCoHost,
		cbsEori,
		bananaCoEori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	if client.DelegationClient == nil {
		t.Fatal("delegation client not set")
	}
	res, err := client.DelegationClient.Get(ctx, newTestDelegationRequestHigh())
	if err != nil {
		t.Errorf("%s", err)
	}
	t.Logf("%#v", res)
}

func TestGetDelegation(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		bananaCoHost,
		cbsEori,
		bananaCoEori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	if client.DelegationClient == nil {
		t.Fatal("delegation client not set")
	}
	res, err := client.DelegationClient.Get(ctx, newTestDelegationRequest())
	if err != nil {
		t.Errorf("%s", err)
	}
	t.Logf("%#v", res)
}
