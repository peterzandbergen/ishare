package ishare

import (
	"context"
	"fmt"

	"gitlab.com/peterzandbergen/ishare/pkg/claims"
	"gitlab.com/peterzandbergen/ishare/pkg/dto"
)

type ExtraService struct {
	BaseService
	Feature dto.FeatureObject
	c       *Client
}

type objectResponse map[string]interface{}

func (o objectResponse) Token() string {
	for _, i := range o {
		if s, ok := i.(string); !ok {
			return ""
		} else {
			return s
		}
	}
	return ""
}

func (c *ExtraService) GetResource(ctx context.Context, path string, resource interface{}) error {
	var response objectResponse
	var clms claims.Claims

	allClaims, ok := resource.(claims.ClaimsGetter)
	if !ok {
		return fmt.Errorf("GetResource: resource does not implement claims.ClaimsGetter")
	}

	if err := c.get(ctx, c.uri, nil, nil, &response, &clms); err != nil {
		return fmt.Errorf("Error getting capabilitiies from %s: %w", c.uri, err)
	}
	// Now get all claims.
	c.c.cfg.UnmarshalJWTUnsafe(response.Token(), allClaims)

	return nil
}
