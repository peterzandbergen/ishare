package ishare

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/dto"
)

var _ TokenResponse = dto.PartiesResponse{}

const iso8601Format = "2006-01-02T15:04:05-0700"

// PartiesInfo is a type alias for the dto version of parties info.
type PartiesInfo = dto.PartiesInfo

type getPartiesOptions map[string]string

type GetPartiesOption func(o getPartiesOptions)

// WithName sets a filter for the name.
//
// Set to "*" to get all parties.
func WithName(filter string) GetPartiesOption {
	return func(o getPartiesOptions) {
		o["name"] = filter
	}
}

// WithEori sets a filter on the EORI of the party.
func WithEori(eori string) GetPartiesOption {
	return func(o getPartiesOptions) {
		o["eori"] = eori
	}
}

// WithPage sets the number of the page to return, starting with 1.
//
// Get returns a maximum of 10 parties per call. The Count properties gives the available number of parties.
// To get all parties, you need to repeat the call or use the GetAll method of the ishare.PartiesService.
func WithPage(p int) GetPartiesOption {
	return func(o getPartiesOptions) {
		o["page"] = strconv.Itoa(p)
	}
}

// WithCertifiedOnly only returns certified parties.
func WithCertifiedOnly() GetPartiesOption {
	return func(o getPartiesOptions) {
		o["certified_only"] = strconv.FormatBool(true)
	}
}

// WithActiveOnly options limits the returned parties to active parties only.
func WithActiveOnly() GetPartiesOption {
	return func(o getPartiesOptions) {
		o["active_only"] = strconv.FormatBool(true)
	}
}

// WithCertificateSubjectName only returns the parties with the given subject name.
func WithCertificateSubjectName(name string) GetPartiesOption {
	return func(o getPartiesOptions) {
		o["certificate_subject_name"] = name
	}
}

// WithDateTime only returns the parties that are valid from the given time.
func WithDateTime(t time.Time) GetPartiesOption {
	return func(o getPartiesOptions) {
		o["date_time"] = t.UTC().Format(iso8601Format)
	}
}

type PartiesService struct {
	BaseService
}

var _ TokenResponse = dto.PartiesResponse{}

// Get returns the parties info.
func (c *PartiesService) Get(ctx context.Context, options ...GetPartiesOption) (*PartiesInfo, error) {
	var response dto.PartiesResponse
	var claims dto.PartiesInfoClaims

	opts := getPartiesOptions{}
	for _, o := range options {
		o(opts)
	}

	if err := c.get(ctx, c.uri, opts, nil, &response, &claims); err != nil {
		return nil, fmt.Errorf("Error getting parties: %w", err)
	}
	return claims.PartiesInfo, nil
}

// GetAll returns all parties.
func (c *PartiesService) GetAll(ctx context.Context, options ...GetPartiesOption) (*PartiesInfo, error) {
	// Overwrite page option.
	opts := append([]GetPartiesOption{}, options...)

	// Get the first set of parties.
	opt := append(opts, WithPage(1))
	res, err := c.Get(ctx, opt...)
	if err != nil {
		return nil, err
	}

	total := int(res.Count)
	fetched := len(res.Data)
	// Repeat for all pages and add the parties to the first result.
	for p := 2; fetched < total; p++ {
		opt := append(opts, WithPage(p))
		r, err := c.Get(ctx, opt...)
		if err != nil {
			return nil, err
		}
		// Append to res.
		res.Data = append(res.Data, r.Data...)
		fetched += len(r.Data)
	}
	return res, nil
}
