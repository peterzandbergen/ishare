package ishare

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/peterzandbergen/ishare/pkg/dto"
	// "gitlab.com/peterzandbergen/ishare/party"
)

var _ TokenResponse = dto.PartyResponse{}

type PartyService struct {
	BaseService
}

// PartyInfo is a type alias for its dto version.
//
// PartyInfo represents a party in the iShare scheme and if the party is valid.
type PartyInfo = dto.Party

// GetPartyOption sets the options for the GetParty request.
type GetPartyOption func(o getPartiesOptions)

const partyIDParam = "{partyId}"

// Get retrieves the party for the given partyID.
func (c *PartyService) Get(ctx context.Context, partyID string, options ...GetPartyOption) (*PartyInfo, error) {
	var response dto.PartyResponse
	var claims dto.PartyInfoClaims

	uri := strings.Replace(c.uri, partyIDParam, partyID, 1)

	// Init opts with mandatory value.
	opts := getPartiesOptions{}
	// add the options.
	for _, o := range options {
		o(opts)
	}

	if err := c.get(ctx, uri, opts, nil, &response, &claims); err != nil {
		return nil, fmt.Errorf("Error getting party: %w", err)
	}
	return claims.PartyInfo, nil
}
