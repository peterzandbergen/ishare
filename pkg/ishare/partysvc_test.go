package ishare

import (
	"context"
	"testing"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
)

func TestPartyFromClient(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		schemeOwnerHost,
		cbsEori,
		schemeOwnerEori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	ci, err := client.PartyClient.Get(context.Background(), warehouse13Eori)
	if err != nil {
		t.Fatalf("error get parties: %s", err)
	}
	_ = ci
}
