package ishare

import (
	"context"
	"fmt"

	"gitlab.com/peterzandbergen/ishare/pkg/dto"
)

var _ TokenResponse = dto.TrustedListResponse{}

type TrustedListService struct {
	BaseService
}

// CertificateAuthority is a type alias for its dto version.
// This prevents the user of the client to import the dto package.
type CertificateAuthority = dto.CertificateAuthority

// Get returns the trusted list from the server.
func (c *TrustedListService) Get(ctx context.Context) ([]CertificateAuthority, error) {
	var response dto.TrustedListResponse
	var claims dto.TrustedListClaims

	if err := c.get(ctx, c.uri, nil, nil, &response, &claims); err != nil {
		return nil, fmt.Errorf("Error getting trusted list: %w", err)
	}
	return claims.CertificateAuthorities, nil
}
