package ishare

import (
	"context"
	"testing"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
)

func TestTrustedList(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	// Do not use context with timeout when debugging.
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := newClient(
		ctx,
		context.Background(),
		schemeOwnerHost,
		cbsEori,
		schemeOwnerEori,
		cert,
		key,
		cas,
		root)
	if err != nil {
		t.Fatalf("error NewClient: %s", err)
	}
	ci, err := client.TrustedListClient.Get(context.Background())
	if err != nil {
		t.Fatalf("error get trusted list: %s", err)
	}
	for _, ca := range ci {
		t.Logf("ca.Subject: %s", ca.Subject)
	}
	_ = ci
}
