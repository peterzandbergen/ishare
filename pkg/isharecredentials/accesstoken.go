package isharecredentials

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
)

const (
	// StdAccessTokenPath is the default path for getting an access token.
	StdAccessTokenPath = "/connect/token"
)

// AccessTokenResponse represents the response of a token request.
type AccessTokenResponse struct {
	AccessToken string  `json:"access_token,omitempty"`
	TokenType   string  `json:"token_type,omitempty"`
	ExpiresIn   float64 `json:"expires_in,omitempty"`
}

// GetAccessToken retrieves an access token from the given host and path.
//
func GetAccessToken(client *http.Client, host string, path string, clientEori string, clientAssertion string) (*AccessTokenResponse, error) {
	// Create URL
	u, err := url.Parse(host)
	if err != nil {
		return nil, err
	}
	u.Path = path
	u.Scheme = "https"
	return GetAccessTokenURI(client, u.String(), clientEori, clientAssertion)
}

func headerContains(header, val string) bool {
	val = strings.ToLower(val)
	header = strings.ToLower(header)
	// Check the content type
	ct := strings.Split(header, "; ")
	for _, elem := range ct {
		if elem == val {
			return true
		}
	}
	return false
}

// GetAccessTokenURI retrieves an access token from the given uri.
func GetAccessTokenURI(client *http.Client, uri string, clientEori string, clientAssertion string) (*AccessTokenResponse, error) {
	// Create the values.
	values := url.Values{}
	values.Set("grant_type", "client_credentials")
	values.Set("scope", "iSHARE")
	values.Set("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer")
	values.Set("client_assertion", clientAssertion)
	values.Set("client_id", clientEori)

	// Create request.
	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(values.Encode()))
	if err != nil {
		return nil, fmt.Errorf("error creating http request: %w", err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Connection", "keep-alive")

	// Create client.
	if client == nil {
		client = &http.Client{}
	}

	// Execute request.
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error executing http request: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return nil, HTTPStatusCodeError(resp)
	}
	// Unmarshal response.
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %w", err)
	}

	// Check content type == application/json
	if ! headerContains(resp.Header.Get("Content-Type"), "application/json") {
		return nil, fmt.Errorf("bad content type, expected 'application/json', got '%s'", resp.Header.Get("Content-Type"))
	}

	res := &AccessTokenResponse{}
	err = json.Unmarshal(b, res)
	if err != nil {
		return nil, fmt.Errorf("error decoding access token json '%s': %w", string(b), err)
	}
	return res, nil
}

// HTTPStatusCodeError returns an error with the body if present.
func HTTPStatusCodeError(r *http.Response) error {
	msg := strings.Builder{}
	if r.Body != nil {
		if n, err := io.Copy(&msg, r.Body); err == nil && n > 0 {
			return fmt.Errorf("http status error: %d %s, body: '%s'", r.StatusCode, http.StatusText(int(r.StatusCode)), msg.String())
		}
	}
	return fmt.Errorf("http status error: %d %s", r.StatusCode, http.StatusText(int(r.StatusCode)))
}
