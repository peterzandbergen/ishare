package isharecredentials

import (
	"net/http"
	"strings"
	"testing"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/token"
)

func TestGetAccessToken(t *testing.T) {
	ct, err := token.NewFromFiles(cbsEori, certCBS, keyCBS, caCertChain, rootCert)
	if err != nil {
		t.Fatalf("error generating token: %s", err)
	}
	// ct.ValidBefore = 3 * time.Second
	ca, err := ct.Assertion(schemeOwnerEori)
	resp, err := GetAccessToken(nil, schemeOwnerHost, StdAccessTokenPath, cbsEori, ca)
	if err != nil {
		t.Fatalf("error get access token: %s", err)
	}
	t.Logf("%#v", resp)
}

func TestGetAccessTokenWarehouse(t *testing.T) {
	ct, err := token.NewFromFiles(cbsEori, certCBS, keyCBS, caCertChain, rootCert)
	if err != nil {
		t.Fatalf("error generating token: %s", err)
	}
	// ct.ValidBefore = 3 * time.Second
	ct.ValidBefore = 3 * time.Second
	ca, err := ct.Assertion(warehouse13Eori)
	resp, err := GetAccessToken(nil, warehouse13Host, StdAccessTokenPath, cbsEori, ca)
	if err != nil {
		t.Fatalf("error get access token: %s", err)
	}
	t.Logf("%#v", resp)
}

func TestMultiHeader(t *testing.T) {
	req, _ := http.NewRequest("GET", "http://example.com", nil)
	req.Header.Add("Header1", "value1")
	req.Header.Add("Header1", "value2")
	req.Header.Set("Header1", "value3")

	b := strings.Builder{}

	req.Write(&b)
	t.Log(b.String())
	// t.Error("\ngen log")
}
