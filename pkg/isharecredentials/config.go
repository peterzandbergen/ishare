package isharecredentials

import (
	"context"
	"crypto/rsa"
	"crypto/x509"
	"fmt"
	"net/http"
	"time"

	"golang.org/x/oauth2"

	"gitlab.com/peterzandbergen/ishare/pkg/claims"
	"gitlab.com/peterzandbergen/ishare/pkg/token"
)

// Config contains the information for a client to server
// client that supports the iShare protocol.
// A client can only communicate with a server with the given EORI.
type Config struct {
	// TokenURL represents the enpoint that provides the iShare tokens.
	TokenURL string

	// ClientEORI represents the identity of the client.
	ClientEORI string

	// ServerEORI represents the identity of the server.
	ServerEORI string

	// Cert and Key are used to sign the JWT client tokens.
	Cert *x509.Certificate
	Key  *rsa.PrivateKey

	// CACerts contains the intermediate certificates for the
	// required outgoing x5c values and to validate incoming JWS tokens.
	CACerts []*x509.Certificate

	// RootCerts contains the root certificates to validate
	// the outgoing and incoming tokens against.
	RootCerts *x509.CertPool

	// TokenBackdate sets the number of seconds to backdate
	// the token's validity interval [iat...exp].
	//
	// The default backdate is 3 seconds before now.
	TokenBackdate int

	// ishareToken
	ct *token.Token
}

// ClientToken returns an existing or new client ishare token.
func (c *Config) ClientToken() (*token.Token, error) {
	if c.ct != nil {
		return c.ct, nil
	}
	// Create new token.
	ct, err := token.New(c.ClientEORI, c.Cert, c.Key, c.CACerts, c.RootCerts)
	if err != nil {
		return nil, err
	}
	if c.TokenBackdate != 0 {
		ct.ValidBefore = time.Duration(c.TokenBackdate) * time.Second
	}
	c.ct = ct
	return c.ct, nil
}

// Client returns an http client that supports iShare oauth.
//
// It uses the http client from the context with oauth2.HTTPClient key for retrieving
// the tokens and for the service endpoint.
//
//	... create your own http client, myClient
//	// add the client to the context
//	ctx := context.WithValue(ctx, oauth2.HTTPClient, myClient)
func (c *Config) Client(ctx context.Context) *http.Client {
	return oauth2.NewClient(ctx, c.TokenSource(ctx))
}

// Token returns an iShare token using the http client from the given context.
func (c *Config) Token(ctx context.Context) (*oauth2.Token, error) {
	return c.TokenSource(ctx).Token()
}

// tokenSource implements wraps config to make it a token source for oauth2.
// It implements the oauth2.TokenSource interface.
type tokenSource struct {
	ctx context.Context
	cfg *Config
}

// TokenSource returns a token source that uses the http client in the context.
// It keeps returning the same token as long as it is not expired.
func (c *Config) TokenSource(ctx context.Context) oauth2.TokenSource {
	source := &tokenSource{
		ctx: ctx,
		cfg: c,
	}
	return oauth2.ReuseTokenSource(nil, source)
}

// Token retrieves a new token from the endpoint.
func (t *tokenSource) Token() (*oauth2.Token, error) {
	var hc *http.Client
	// Get the http client from the context.
	if c, ok := t.ctx.Value(oauth2.HTTPClient).(*http.Client); ok {
		hc = c
	} else {
		hc = &http.Client{}
	}
	ct, err := t.cfg.ClientToken()
	if err != nil {
		return nil, fmt.Errorf("error getting ishare client token: %w", err)
	}
	assertion, err := ct.Assertion(t.cfg.ServerEORI)
	if err != nil {
		return nil, fmt.Errorf("error getting assertion: %w", err)
	}
	// log.Printf("assertion:\n%s", assertion)
	resp, err := GetAccessTokenURI(hc, t.cfg.TokenURL, t.cfg.ClientEORI, assertion)
	if err != nil {
		return nil, fmt.Errorf("error retrieving access token: %w", err)
	}
	return &oauth2.Token{
		AccessToken:  resp.AccessToken,
		TokenType:    resp.TokenType,
		RefreshToken: "",
		Expiry:       time.Now().Add(time.Duration(int64(resp.ExpiresIn)) * time.Second),
	}, nil
}

// UnmarshalJWT unmarshals the signed token to the claims structure.
// It passes the call to UnmarshalJWT on the client token.
//
// This is provided as a convenience function. It is also possible to
// get the token with ClientToken and call UnMarshalTWT on it.
func (c *Config) UnmarshalJWT(signedJWT string, claims claims.ClaimsGetter) error {
	ct, err := c.ClientToken()
	if err != nil {
		return err
	}
	return ct.UnmarshalJWT(signedJWT, claims)
}

// UnmarshalJWTUnsafe calls UnmarshalJWTUnsafe on the client token.
//
// This is provided as a convenience function. It is also possible to
// get the token with ClientToken and call UnmarshalJWTUnsafe on it.
func (c *Config) UnmarshalJWTUnsafe(signedJWT string, claims claims.ClaimsGetter) error {
	ct, err := c.ClientToken()
	if err != nil {
		return err
	}
	return ct.UnmarshalJWTUnsafe(signedJWT, claims)
}

// Assertion returns an iShare Assertion that can be used to retrieve an access token.
func (c *Config) Assertion() (string, error) {
	ct, err := c.ClientToken()
	if err != nil {
		return "", err
	}
	res, err := ct.Assertion(c.ServerEORI)
	if err != nil {
		return "", err
	}
	return res, nil
}
