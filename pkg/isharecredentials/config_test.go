package isharecredentials

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"testing"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
	"gitlab.com/peterzandbergen/ishare/pkg/dto"
)

const (
	cbsEori = "EU.EORI.NL51197073"
	certCBS = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem"
	keyCBS  = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem"

	caCertChain = "/home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem"
	rootCert    = "/home/peza/Documents/iShare/iShareTestKey2/root.pem"

	schemeOwnerHost            = "https://scheme.isharetest.net"
	schemeOwnerTokenURI        = "https://scheme.isharetest.net/connect/token"
	schemeOwnerCapabilitiesURI = "https://scheme.isharetest.net/capabilities"
	schemeOwnerEori            = "EU.EORI.NL000000000"

	warehouse13Host = "https://w13.isharetest.net"
	warehouse13Eori = "EU.EORI.NL000000003"

	authorizationRegistryHost = "https://ar.isharetest.net"
	authorizationRegistryEori = "EU.EORI.NL000000004"
)

func TestConfigTokenSource(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}

	cfg := &Config{
		ClientEORI: cbsEori,
		ServerEORI: schemeOwnerEori,
		TokenURL:   schemeOwnerTokenURI,
		Cert:       cert,
		CACerts:    cas,
		RootCerts:  root,
		Key:        key,
	}
	ts := cfg.TokenSource(context.Background())
	_, err = ts.Token()
	if err != nil {
		t.Fatalf("cannot get token from token source: %s", err)
	}

	tk, err := cfg.Token(context.Background())
	if err != nil {
		t.Fatalf("cannot get token: %s", err)
	}
	_ = tk
	b, err := json.MarshalIndent(tk, "", "    ")
	if err != nil {
		t.Fatalf("error mashaling token")
	}
	t.Logf("token:\n%s\n", string(b))
}

func TestConfigClient(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}

	cfg := &Config{
		ClientEORI: cbsEori,
		ServerEORI: schemeOwnerEori,
		TokenURL:   schemeOwnerTokenURI,
		Cert:       cert,
		CACerts:    cas,
		RootCerts:  root,
		Key:        key,
	}
	client := cfg.Client(context.Background())
	resp, err := client.Get(schemeOwnerCapabilitiesURI)
	if err != nil {
		t.Fatalf("error get: %s", err)
	}
	jtb, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("error reading body: %s", err)
	}
	caps := dto.CapabilitiesResponse{}
	if err := json.Unmarshal(jtb, &caps); err != nil {
		t.Fatalf("error decoding payload: %s", err)
	}

	var claims dto.CapabilitiesInfoClaims
	if err := cfg.UnmarshalJWT(caps.CapabilitiesToken, &claims); err != nil {
		t.Fatalf("error decoding jwt: %s", err)
	}
	t.Logf("claims: %v", claims)
	_ = resp
}
