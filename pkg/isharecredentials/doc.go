/*
Package isharecredentials implements the token source for the iShare oauth2 scheme.

The client credentials are designed to be used in combination with the golang.org/x/oauth2 package.

iShare tokens use the x5c header to prove their identity and have additional requirements for the
headers in the token used to obtain the access token. 

More information is available on the iShare developers 
website at https://dev.ishareworks.org/m2m/authentication.html#authentication.

isharecredentials follows the same approach as the clientcredentials package in golang.org/x/oauth2. 
You create a Config with the correct settings and create a http.Client with the Client all.

	// Create config
	cfg := &Config{
		ClientEORI: cbsEori,
		ServerEORI: schemeOwnerEori,
		TokenURL:   schemeOwnerTokenURI,
		Cert:       cert,
		CACerts:    cas,
		RootCerts:  root,
		Key:        key,
	}
	// Create the http.Client
	client := cfg.Client(ctx)

You can pass your own http.Client in the context as follows.

	// Use myClient as basis by passing it via the context.
	ctx := context.WithValue(ctx, oauth2.HTTPClient, myClient)
	// Create the http.Client
	client := cfg.Client(ctx)

*/
package isharecredentials