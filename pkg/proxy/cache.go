package proxy

import (
	"context"

	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"

	"net/http"
	"sync"

	"github.com/go-logr/logr"
)

type clientCache struct {
	// icfg contains the configuration for the iShare clients.
	icfg *isharecredentials.Config

	// iClients contains a cache of http.Clients.
	iClients map[string]*http.Client

	// iClientsMutex guards the iClients map.
	iClientsMutex sync.RWMutex

	log logr.Logger
}

func newClientCache(icfg *isharecredentials.Config, log logr.Logger) *clientCache {
	return &clientCache{
		icfg:     icfg,
		log:      log,
		iClients: map[string]*http.Client{},
	}
}

// getClientFromCache retrieves client from cache or nil if not present.
func (c *clientCache) getClientFromCache(key string) *http.Client {
	c.iClientsMutex.RLock()
	defer c.iClientsMutex.RUnlock()
	// Try the cache.
	client, ok := c.iClients[key]
	if ok {
		return client
	}
	return nil
}

// getClient returns a client for the host and eori.
func (c *clientCache) getClient(ctx context.Context, tokenURL, serverEORI string) (*http.Client, error) {
	// Build the key.
	key := tokenURL + " " + serverEORI

	// Try the cache.
	if res := c.getClientFromCache(key); res != nil {
		return res, nil
	}

	// Cache miss.
	// Start w lock.
	c.iClientsMutex.Lock()
	defer c.iClientsMutex.Unlock()

	// Try again for someone else could have done it for us.
	client, ok := c.iClients[key]
	if ok {
		return client, nil
	}

	// Still no client for our host, create a new one and add to cache.
	cfgCopy := isharecredentials.Config{
		ClientEORI:    c.icfg.ClientEORI,
		Cert:          c.icfg.Cert,
		Key:           c.icfg.Key,
		CACerts:       c.icfg.CACerts,
		RootCerts:     c.icfg.RootCerts,
		TokenBackdate: c.icfg.TokenBackdate,
		TokenURL:      tokenURL,
		ServerEORI:    serverEORI,
	}
	client = cfgCopy.Client(context.Background())
	c.iClients[key] = client
	return client, nil
}
