package proxy

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"net/http"
	"strings"
)

func HasContentEncoding(h http.Header, encoding string) bool {
	encoding = strings.ToLower(encoding)
	enc := strings.ToLower(h.Get("Content-Encoding"))
	return strings.Compare(enc, encoding) == 0
}

func GUnzip(d []byte) ([]byte, error) {
	r, err := gzip.NewReader(bytes.NewBuffer(d))
	if err != nil {
		return nil, fmt.Errorf("error creating gzip reader: %w", err)
	}
	res, err := io.ReadAll(r)
	if err != nil {
		return nil, fmt.Errorf("error ungzipping content: %w", err)
	}
	return res, nil
}

func DecodeContent(h http.Header, body []byte) ([]byte, error) {
	enc := strings.ToLower(h.Get("Content-Encoding"))
	switch enc {
	case "":
		return body, nil
	case "gzip":
		return GUnzip(body)
	default:
		return nil, fmt.Errorf("unsupported content encoding: %s", enc)
	}
}
