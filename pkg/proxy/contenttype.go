package proxy

import (
	"net/http"
	"strings"
)

// IsContentType checks if the value is of cType.
// According to RFC 1341 the type subtype value is case insensitive.
func IsContentType(value string, typeSubtype string) bool {
	typeSubtype = strings.ToLower(typeSubtype)
	value = strings.ToLower(value)
	return strings.HasPrefix(value, typeSubtype)
}

func HasContentType(header http.Header, typeSubtype string) bool {
	cth := header.Values("Content-Type")
	for _, v := range cth {
		if IsContentType(v, typeSubtype) {
			return true
		}
	}
	return false
}
