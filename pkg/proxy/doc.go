/*
Package proxy implements a proxy server that forwards http requests with iShare Bearer tokens.

The requests need to provide the extra information in the HTTP headers. The headers are specified as constants.

A typical request requires the headers

	X-Ishare-Adapter-Key: some-very-secret-key
	X-Ishare-Server-Eori: 00000000001
	X-Ishare-Token-Url:   scheme.ishare.net
	X-Ishare-Service-Url: http://scheme.ishare.net/capabilities

When a client wants to send a get request to e.g. https://scheme.ishare.net/capabilities,
then it needs to provide the parameters in the headers as shown above.

The proxy clones the request except for the headers shown above, 
and sends it to the url passed in "X-Ishare-Server-Resource".

The server caches the iShare capable clients to limit the token requests.

*/
package proxy
