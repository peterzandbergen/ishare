package proxy

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"
	"gitlab.com/peterzandbergen/ishare/pkg/token"

	"github.com/go-logr/logr"
)

const (
	// HeaderAPIKey is the header for the apiKey
	HeaderAPIKey = "X-Ishare-Adapter-Key"
	// HeaderTokenURL is the header name that must contain the base url of the server.
	// It is used in the iShare Config.
	HeaderTokenURL = "X-Ishare-Token-Url"
	// HeaderServerEORI contains the EORI of the server.
	HeaderServerEORI = "X-Ishare-Server-Eori"
	// HeaderResource contains the resource the client wants to send the request to.
	// This must be a valid url.
	HeaderServiceURL = "X-Ishare-Service-Url"
)

// A Server implements the proxy.
type Server struct {
	// handler holds the
	*http.ServeMux

	// apiKey to protect the handler endpoint.
	apiKey string

	cache *clientCache

	// logger
	log logr.Logger
}

// New creates a new proxy server.
//
//   - addr is the addres that the proxy will listen on.
//
//   - apiKey, if specified, protects the adapter with an api key that must be specified in the X-Ishare-Adapter-Key header.
//
//   - log specifies the zap sugared logger. Can be nil for no logging.
//
//   - icfg must be an isharecredentials.Config, see below
//
//     icfg is used as a template, the following fields are used:
//
//   - ClientEORI
//
//   - Cert
//
//   - Key
//
//   - CACerts
//
//   - RootCerts
//
//   - TokenBackdate
func New(icfg *isharecredentials.Config, apiKey string, log logr.Logger) *Server {
	s := &Server{
		apiKey: apiKey,
		cache:  newClientCache(icfg, log),
	}
	s.log = log.WithName("server")
	s.routes()
	return s
}

// copyRequest creates a new request from r.
// It copies the Accept header.
func (s *Server) copyRequest(r *http.Request) (*http.Request, error) {
	resource := r.Header.Get(HeaderServiceURL)
	if resource == "" {
		return nil, fmt.Errorf("%s header is empty", HeaderServiceURL)
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, fmt.Errorf("error creating reading client request body: %w", err)
	}
	// Create the request
	req, err := http.NewRequest(r.Method, resource, bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("error creating new request: %w", err)
	}
	// Count the proxy relevant headers.
	n := 0
	for k := range r.Header {
		switch k {
		// Skip these headers.
		case HeaderAPIKey, HeaderServiceURL, HeaderServerEORI, HeaderTokenURL:
			n++
		default:
			// Do not copy the headers for now.
			// ishare does not require special headers.
		}
	}
	if n < 3 {
		return nil, fmt.Errorf("header(s) missing, expected 3 or more, counted: %d", n)
	}
	copyRequestHeaders(req.Header, r.Header)
	return req, nil
}

// copyResponse copies the response from r to w.
// It copies the content type header.
func (s *Server) copyResponse(w http.ResponseWriter, r *http.Response, body []byte) {
	l1 := len(body)

	if HasContentType(r.Header, "application/json") {
		// Get the payload.
		b, err := s.extractToken(body)
		if err != nil {
			s.log.Error(err, "error extracting json token", HeadersKV(r.Header)...)
			http.Error(w, fmt.Sprintf("bad payload: %s", err), http.StatusInternalServerError)
			return
		}
		body = b
	}
	if r.StatusCode < 200 || r.StatusCode >= 300 {
		s.log.Info("status of client request", "statusCode", r.StatusCode, "url", r.Request.URL.String(), "body", string(body))
	}

	// Copy the content type header if present.
	copyResponseHeaders(w.Header(), r.Header)

	// Set the status code.w.WriteHeader(sc)
	l2 := len(body)
	if l1 != l2 {
		s.log.Info("body size changed", "body_l1", l1, "body_l2", l2)
	}

	// Pass the status code on.
	w.WriteHeader(r.StatusCode)
	i, _ := w.Write(body)
	// Try to flush.
	if f, ok := w.(http.Flusher); ok {
		f.Flush()
		s.log.Info("called flush on Flusher")
	}
	s.log.Info("wrote body", "bytes_written", i)
}

// HeadersKV returns key-value pairs for the headers.
// The name of the key is header.<header name>.<header index>
//
// e.g header "content-encoding: gzip; deflate" produces
// "header.content-encoding.0", "gzip", "header.content-encoding.1", "deflate"
func HeadersKV(h http.Header) []any {
	var res []any
	for k, v := range h {
		// Loop over header values.
		for i := 0; i < len(v); i++ {
			res = append(res, fmt.Sprintf("response.header.%s.%d", k, i), v[i])
		}
	}
	return res
}

// isAuthenticated returns true if the request is authenticated.
func (s *Server) isAuthenticated(r *http.Request) bool {
	if s.apiKey != "" {
		return true
	}
	key := r.Header.Get(HeaderAPIKey)
	return key != s.apiKey
}

// requestHandler returns the handler.
func (s *Server) requestHandler() http.HandlerFunc {
	// HandlerFunc
	return func(w http.ResponseWriter, r *http.Request) {
		log := s.log.WithValues("handler", "requestHandler")
		log.Info("request received",
			"serviceUrl", r.Header.Get(HeaderServiceURL),
			"tokenUrl", r.Header.Get(HeaderTokenURL),
			"serverEori", r.Header.Get(HeaderServerEORI),
		)
		// Check for api key if set.
		if s.isAuthenticated(r) {
			log.Info("unauthorized request")
			http.Error(w, "unauthorized", http.StatusUnauthorized)
			return
		}
		// Create the request.
		req, err := s.copyRequest(r)
		if err != nil {
			log.Error(err, "copy request error")
			http.Error(w, fmt.Sprintf("bad request: %s", err), http.StatusBadRequest)
			return
		}

		// Get a client for the server and EORI.
		c, err := s.cache.getClient(r.Context(), r.Header.Get(HeaderTokenURL), r.Header.Get(HeaderServerEORI))
		if err != nil {
			log.Error(err, "error getting client")
			http.Error(w, fmt.Sprintf("error getting iShare client: %s", err), http.StatusInternalServerError)
			return
		}

		// Log the request headers.
		s.log.Info("request headers before Do", HeadersKV(r.Header)...)

		// Execute the request.
		resp, err := c.Do(req)
		if err != nil {
			log.Error(err, "error Do request")
			http.Error(w, fmt.Sprintf("error consuming service: %s", err), http.StatusInternalServerError)
			return
		}

		// Get the response body.
		b, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Error(err, "error reading body")
			http.Error(w, fmt.Sprintf("error reading body: %s", err), http.StatusInternalServerError)
			return
		}

		s.log.Info("executed http.Do",
			"content_type", resp.Header.Get("Content-Type"),
			"status_code", resp.StatusCode,
			"body_len", len(b),
			"url", req.URL.String(),
		)

		// Return the result. This also ends the request.
		s.copyResponse(w, resp, b)
		log.Info("request served")
	}
}

func (s *Server) unmarshalResponse(b []byte) (interface{}, error) {
	tk, err := s.cache.icfg.ClientToken()
	if err != nil {
		return nil, err
	}

	var obj interface{}
	err = json.Unmarshal(b, &obj)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling data: %w", err)
	}

	// Try to get a token.
	tr, err := token.IsTokenResponse(obj)
	if err != nil {
		// Not a token
		s.log.Info("received non token response")
		return obj, nil
	}
	s.log.Info("received token response")
	return tk.GetTokenClaims(tr)
}

func (s *Server) extractToken(body []byte) ([]byte, error) {
	i, err := s.unmarshalResponse(body)
	if err != nil {
		return nil, err
	}
	res, err := json.Marshal(i)
	if err != nil {
		return nil, err
	}
	return res, nil
}

// routes create the paths.
func (s *Server) routes() {
	s.ServeMux = http.NewServeMux()
	s.HandleFunc("/", s.requestHandler())
	s.log.Info("handler for / registered", "method", "routes")
}

func copyHeaders(dst, src http.Header, names ...string) {
	for _, name := range names {
		copyHeader(dst, src, name)
	}
}

func copyHeader(dst, src http.Header, name string) {
	if v := src.Get(name); v != "" {
		dst.Set(name, v)
	}
}

func copyRequestHeaders(dst, src http.Header) {
	headers := []string{
		"Accept",
		"Content-Type",
	}
	copyHeaders(dst, src, headers...)
}

func copyResponseHeaders(dst, src http.Header) {
	headers := []string{
		"Content-Type",
	}
	copyHeaders(dst, src, headers...)
}
