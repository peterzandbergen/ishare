package proxy

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
	"gitlab.com/peterzandbergen/ishare/pkg/isharecredentials"

	"github.com/go-logr/stdr"
)

const (
	cbsEori = "EU.EORI.NL51197073"
	certCBS = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem"
	keyCBS  = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem"

	caCertChain = "/home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem"
	rootCert    = "/home/peza/Documents/iShare/iShareTestKey2/root.pem"

	schemeOwnerHost            = "https://scheme.isharetest.net"
	schemeOwnerTokenURI        = "https://scheme.isharetest.net/connect/token"
	schemeOwnerCapabilitiesURI = "https://scheme.isharetest.net/capabilities"
	schemeOwnerEori            = "EU.EORI.NL000000000"

	warehouse13Host = "https://w13.isharetest.net"
	warehouse13Eori = "EU.EORI.NL000000003"

	authorizationRegistryHost = "https://ar.isharetest.net"
	authorizationRegistryEori = "EU.EORI.NL000000004"

	bananaCoEori     = "EU.EORI.NL000000005"
	bananaCoHost     = "https://banana.isharetest.net"
	bananaCoTokenUri = "https://banana.isharetest.net/connect/token"

	awesomeWidgetsHost    = "https://awesome.isharetest.net"
	awesomeWidgetsEori    = "EU.EORI.NL000000002"
	awesomeWidgetTokenUri = "https://awesome.isharetest.net/connect/token"
)

func TestShutdown(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_ = ctx
}

func TestProxy(t *testing.T) {
	cert, err := certutil.ParseCertFromPEMFile(certCBS)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	key, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyCBS)
	if err != nil {
		t.Fatalf("cannot load key file: %s", err)
	}
	cas, err := certutil.ParseCertsFromFile(caCertChain)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}
	root, err := certutil.ParseCertPoolFromPEMFile(rootCert)
	if err != nil {
		t.Fatalf("cannot load pem file: %s", err)
	}

	cfg := &isharecredentials.Config{
		ClientEORI: cbsEori,
		Cert:       cert,
		Key:        key,
		CACerts:    cas,
		RootCerts:  root,
	}
	prx := New(cfg, "", stdr.New(nil))
	srv := httptest.NewUnstartedServer(prx)
	srv.Start()
	url := srv.URL
	defer srv.Close()

	req, err := http.NewRequest(http.MethodGet, url, nil)
	// req.Header.Set(HeaderServerEORI, bananaCoEori) // Bad eori
	req.Header.Set(HeaderServerEORI, awesomeWidgetsEori)
	req.Header.Set(HeaderServiceURL, awesomeWidgetsHost+"/me")
	req.Header.Set(HeaderTokenURL, awesomeWidgetTokenUri)
	req.Header.Set("Accept", "application/json")
	if err != nil {
		t.Fatalf("error creating request: %s", err)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Do error: %s", err)
	}
	// msg := strings.Builder{}
	body, _ := io.ReadAll(resp.Body)
	l := len(body)
	t.Logf("received %d bytes", l)
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		t.Fatalf("status code error: %d, %s, message: %s", resp.StatusCode, http.StatusText(resp.StatusCode), string(body))
	}
	t.Errorf("msg: %s", string(body))
}
