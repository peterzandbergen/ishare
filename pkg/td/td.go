package td

import (
	"fmt"
	"net/http"
	"net/http/httputil"

	"github.com/go-logr/logr"
)

type TransportDumper struct {
	http.RoundTripper

	logger logr.Logger
}

type Client struct {
	http.Client
}

func (t *TransportDumper) RoundTrip(r *http.Request) (*http.Response, error) {
	b, err := httputil.DumpRequestOut(r, true)
	if err == nil {
		msg := fmt.Sprintf("\n----- BEGIN Outgoing request -----\n%s\n----- END Outgoing request -----\n", string(b))
		t.logger.Info(msg)
	}
	return t.RoundTripper.RoundTrip(r)
}

func New(next http.RoundTripper, logger logr.Logger) *TransportDumper {
	if next == nil {
		next = http.DefaultTransport
	}
	return &TransportDumper{
		RoundTripper: next,
		logger:       logger,
	}
}

func InjectLogger(client *http.Client, logger logr.Logger) {
	client.Transport = New(client.Transport, logger)
}
