package token

import (
	"github.com/google/uuid"
)

var NonceSource nonceSource

type nonceSource struct{}

func (n nonceSource) Nonce() (string, error) {
	return uuid.New().String(), nil
}
