package token

import (
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"

	"time"

	"gitlab.com/peterzandbergen/ishare/pkg/certutil"
	"gitlab.com/peterzandbergen/ishare/pkg/claims"

	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

var (
	ErrKeyIsNotRSA          = errors.New("key is not of type RSA")
	ErrTokenInvalid         = errors.New("token invalid")
	ErrNoPemBlocksFound     = errors.New("no pem blocks found")
	ErrBadX5CEntry          = errors.New("bad x5c entry")
	ErrNoRootFound          = errors.New("no root found")
	ErrNoCertFound          = errors.New("no cert found")
	ErrNoCertsFound         = errors.New("no certs found")
	ErrNoIntermediatesFound = errors.New("no intermediates found")
	ErrNoLeafFound          = errors.New("no leaf found")
)

// Token contains the settings required to generate and validate iShare compliant jwt tokens.
type Token struct {
	ClientEori   string
	ServerEori   string
	Cert         *x509.Certificate
	CaChainCerts []*x509.Certificate
	CaChain      *x509.CertPool
	RootCerts    *x509.CertPool
	X5c          [][]byte
	privateKey   interface{}
	ValidBefore  time.Duration
	ValidFor     time.Duration
}

// New creates a new ishare token.
//
// caChain can be nil.
func New(clientEori string, cert *x509.Certificate, privateKey interface{}, caChainCerts []*x509.Certificate, rootCerts *x509.CertPool) (*Token, error) {
	// Get the chain.
	if cert == nil {
		return nil, ErrNoCertFound
	}
	if privateKey == nil {
		return nil, ErrKeyIsNotRSA
	}
	caChain := certutil.PoolFromCerts(caChainCerts)
	chains, err := cert.Verify(x509.VerifyOptions{
		Intermediates: caChain,
		Roots:         rootCerts,
	})
	if err != nil {
		return nil, fmt.Errorf("New: error verifying ca chain: %w", err)
	}
	// x5cCerts := make([]*x509.Certificate, len(chains)+1)
	// x5cCerts[0] = cert
	// copy(x5cCerts[1:], chains[0])
	x5c := certutil.RawCerts(chains[0])
	return &Token{
		ClientEori:   clientEori,
		Cert:         cert,
		CaChain:      caChain,
		CaChainCerts: caChainCerts,
		X5c:          x5c,
		privateKey:   privateKey,
		RootCerts:    rootCerts,
		ValidBefore:  3 * time.Second,
		ValidFor:     30 * time.Second,
	}, nil
}

// NewFromFile creates a new token and accepts the names of the files that contain the keys and certificates.
func NewFromFiles(clientEori, certFile, keyFile, caCertsFile, rootCertsFile string) (*Token, error) {
	cert, err := certutil.ParseCertFromPEMFile(certFile)
	if err != nil {
		return nil, fmt.Errorf("NewFromFiles: error loading cert file %s: %w", certFile, err)
	}
	pk, err := certutil.ParseRSAPrivateKeyFromPEMFile(keyFile)
	if err != nil {
		return nil, err
	}
	caCerts, err := certutil.ParseCertsFromFile(caCertsFile)
	if err != nil {
		return nil, fmt.Errorf("NewFromFiles: error loading ca chain cert pool file %s: %w", caCertsFile, err)
	}
	roots, err := certutil.ParseCertPoolFromPEMFile(rootCertsFile)
	if err != nil {
		return nil, fmt.Errorf("NewFromFiles: error loading root cert pool from file %s: %w", rootCertsFile, err)
	}
	if err != nil {
		return nil, fmt.Errorf("error loading X5C from file: %w", err)
	}
	return New(clientEori, cert, pk, caCerts, roots)
}

// Assertion returns an assertion string to obtain a new iShare OAuth2 token.
func (t *Token) Assertion(audience string) (string, error) {
	claims := t.NewClaims(audience)

	return t.getToken(&claims, t.X5c, t.privateKey)
}

func (t *Token) NewClaims(audience string) claims.Claims {
	validFrom := time.Now().Add(-t.ValidBefore)
	now := validFrom
	exp := now.Add(t.ValidFor)
	// Create the claims accoring to iShare rules.
	claims := claims.Claims{
		Subject:  t.ClientEori,
		Issuer:   t.ClientEori,
		Audience: claims.Audience{audience},
		ID:       uuid.New().String(),
		IssuedAt: claims.NewNumericDate(now),
		Expiry:   claims.NewNumericDate(exp),
	}
	return claims
}

// ResponseToken returns the compact signed JWT with the given claims.
//
// To create your own response token, you need to provide your own struct that embeds
// a jwt.Claims that is obtained using the NewClaims method.
//
//		Claims := t.NewClaims(audienceEORI)
//	 claims := struct {
//
// }
func (t *Token) ResponseToken(claims claims.ClaimsGetter) (string, error) {
	return t.getToken(claims, t.X5c, t.privateKey)
}

// getToken returns an iShare compliant client assertion.
//
//	validFrom is the time from when the token is valid.
//	validFor is the duration of the validity of the assertion, this should be 30 seconds.
//	x5c is the array of certificates, the client cert first and the ordered.
//	pk is the private key.
func (t *Token) getToken(claims claims.ClaimsGetter, x5c [][]byte, pk interface{}) (string, error) {
	jc := NewValidatingClaims(claims, nil)
	jt := jwt.NewWithClaims(jwt.SigningMethodRS256, jc)
	jt.Header["x5c"] = x5c

	return jt.SignedString(pk)
}

func (t *Token) getKey(jt *jwt.Token) (interface{}, error) {
	x5c := getHeaderByteArrays(jt.Header, "x5c")
	if x5c == nil {
		return nil, fmt.Errorf("Token.getKey: x5c header missing")
	}
	leaf, err := x509.ParseCertificate(x5c[0])
	if err != nil {
		return nil, fmt.Errorf("Token.getKey: error parsing x5c[0]: %w", err)
	}
	return leaf.PublicKey, nil
}

// UnmarshalJWT unmarshals and validates the signed JWT.
//
// The validation checks if the token complies with the iShare rules.
// See for more details: https://dev.ishareworks.org/introduction/jwt.html#ishare-jwt
func (t *Token) UnmarshalJWT(signedJWT string, claims claims.ClaimsGetter, options ...validateOption) error {
	vc := NewValidatingClaims(claims, nil)
	jt, err := jwt.ParseWithClaims(signedJWT, vc, t.getKey)
	if err != nil {
		return fmt.Errorf("error parsing the jwt with claims: %w", err)
	}

	// Validate the headers and extract the leaf certificate.
	cert, err := t.ValidateHeaders(jt.Header)
	if err != nil {
		return fmt.Errorf("error validating headers: %w", err)
	}
	// Validate and get the claims.
	err = t.ValidateClaims(claims, cert.Subject.SerialNumber, options...)
	if err != nil {
		return fmt.Errorf("error validating the claims: %w", err)
	}
	return nil
}

func (t *Token) UnmarshalJWTUnsafe(signedJWT string, claims claims.ClaimsGetter) error {
	_, err := jwt.Parse(signedJWT, t.getKey)
	if err != nil {
		return fmt.Errorf("error parsing the jwt: %w", err)
	}
	p := jwt.Parser{}
	_, _, err = (&p).ParseUnverified(signedJWT, NewValidatingClaims(claims, nil))
	if err != nil {
		return fmt.Errorf("error parsing token without verfication: %w", err)
	}
	return nil
}

func getHeaderString(header map[string]interface{}, key string) string {
	// get the header
	v := header[key]
	if v == nil {
		return ""
	}

	if val, ok := v.(string); ok {
		return val
	}
	return ""
}

func getHeaderByteArrays(header map[string]interface{}, key string) [][]byte {
	h := header[key]
	if h == nil {
		return nil
	}
	hh, ok := h.([]interface{})
	if !ok {
		return nil
	}

	var res [][]byte
	for _, x := range hh {
		s, ok := x.(string)
		if !ok {
			return nil
		}
		// Convert base64
		b, err := base64.StdEncoding.DecodeString(s)
		if err != nil {
			return nil
		}
		res = append(res, b)
	}
	return res
}

// validateX5C validates the x5c header and checks and verifies the leaf.
//
// The intermediates used in the verify options are a combination of the ones
// that were passed when creating the token and the certs that are in the x5c header.
func (t *Token) validateX5C(x5c [][]byte) (*x509.Certificate, error) {
	if len(x5c) == 0 {
		return nil, fmt.Errorf("x5c is empty")
	}
	// Get the cert.
	leaf, err := x509.ParseCertificate(x5c[0])
	if err != nil {
		return nil, fmt.Errorf("error parsing leaf: %w", err)
	}
	// Build intermediate pool with remaining xc5 elements.
	im := x509.NewCertPool()
	for i, bc := range x5c[1:] {
		c, err := x509.ParseCertificate(bc)
		if err != nil {
			return nil, fmt.Errorf("error parsing x5c[%d]: %w", i, err)
		}
		im.AddCert(c)
	}
	// Add ca chain certs.
	for _, c := range t.CaChainCerts {
		im.AddCert(c)
	}
	// Validate the cert with combined intermediates and root.
	chains, err := leaf.Verify(x509.VerifyOptions{
		Intermediates: im,
		Roots:         t.RootCerts,
	})
	if err != nil {
		return nil, fmt.Errorf("error checking certificates: %w", err)
	}
	return chains[0][0], nil
}

// ValidateHeaders checks if the JWT headers are iShare compliant.
//
//   - only typ, alg and x5c are present
//   - typ == JWT
//   - alg == RS256
//   - x5c certificates are valid
func (t *Token) ValidateHeaders(header map[string]interface{}) (*x509.Certificate, error) {
	// get the typ header
	typ := getHeaderString(header, "typ")
	if typ != "JWT" {
		return nil, fmt.Errorf("typ header not equal to JWT: %s", typ)
	}
	// Check algorithm
	alg := getHeaderString(header, "alg")
	if alg != jwt.SigningMethodRS256.Alg() {
		return nil, fmt.Errorf("alg not equal to %s: %s", jwt.SigningMethodES256.Alg(), alg)
	}

	bc := getHeaderByteArrays(header, "x5c")
	if bc == nil {
		return nil, fmt.Errorf("x5c header is missing")
	}

	res, err := t.validateX5C(bc)
	if err != nil {
		return nil, fmt.Errorf("error validating xc5 header: %w", err)
	}
	return res, nil
}

type validateOptions struct {
	disableIssuerSubjectEquality bool
	// caCerts
}

type validateOption func(*validateOptions)

// WithDisableIssuerSubjectEquality disables the check if the issuer is equal to the cert subject name.
func WithDisableIssuerSubjectEquality() validateOption {
	return func(o *validateOptions) {
		o.disableIssuerSubjectEquality = true
	}
}

// ValidateClaims checks if the received token is valid.
// It checks
//   - JTW Headers for only the presence of alg, typ and xc5.
//   - Valid Cert chain.
//   - Cert.Subject.serialNumber value is equal to the identity of the creator of the JWT.
//     For a request this must be the client EORI (iss and sub), for a response this must be the server (aud) EORI.
//   - Mandatory JWT claims: iss, sub, aud, jti, exp, iat.
//   - iss and sub must be equal.
//   - Valid duration, exp - iat, must be 30 seconds and now must be in the interval.
func (t *Token) ValidateClaims(claims claims.ClaimsGetter, subjectSerialNumber string, options ...validateOption) error {
	opt := validateOptions{}
	for _, o := range options {
		o(&opt)
	}

	// Check the claims.
	sc := claims.GetClaims()
	// Check valid time difference
	if p := *sc.Expiry - *sc.IssuedAt; p != 30 {
		return fmt.Errorf("valid period not equal to 30 seconds: %d", p)
	}
	// jti must be present
	if sc.ID == "" {
		return fmt.Errorf("jti is missing")
	}
	// Check for presences of iat and exp
	if sc.IssuedAt == nil {
		return fmt.Errorf("iat is missing")
	}
	if sc.Expiry == nil {
		return fmt.Errorf("exp is missing")
	}
	// Check the freshness of the token
	now := time.Now().Add(time.Second)
	if now.Before(sc.IssuedAt.Time()) || now.After(sc.Expiry.Time()) {
		return fmt.Errorf("token is stale")
	}
	// Issuer and subject should be equal
	if !opt.disableIssuerSubjectEquality && sc.Issuer != sc.Subject {
		return fmt.Errorf("issuer and subject should be equal: iss: %s, sub: %s", sc.Issuer, sc.Subject)
	}
	// Issuer must be equal to the certificate subject serial number.
	if sc.Issuer != subjectSerialNumber {
		return fmt.Errorf("issuer and cert subject serial number do not match: iss %s, serial number: %s", sc.Issuer, subjectSerialNumber)
	}
	return nil
}

type ValidatingClaims struct {
	claims.ClaimsGetter
	valid func() error
}

func (c *ValidatingClaims) Valid() error {
	return c.valid()
}

func NewValidatingClaims(claims claims.ClaimsGetter, valid func() error) jwt.Claims {
	if valid == nil {
		valid = func() error { return nil }
	}
	return &ValidatingClaims{ClaimsGetter: claims, valid: valid}
}

func (c *ValidatingClaims) UnmarshalJSON(d []byte) error {
	return json.Unmarshal(d, c.ClaimsGetter)
}

func (c *ValidatingClaims) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.ClaimsGetter)
}

type TokenResponse struct {
	Token interface{}
	JWT   interface{}
}

type fakeClaims map[string]interface{}

func (f fakeClaims) GetClaims() claims.Claims {
	return claims.Claims{}
}

func IsTokenResponse(tk interface{}) (tokenString string, err error) {
	// Cast to map.
	obj, ok := tk.(map[string]interface{})
	if !ok {
		return "", fmt.Errorf("tk is not an object, may be an array")
	}

	if l := len(obj); l != 1 {
		// Not a token.
		return "", fmt.Errorf("expected one property, found %d", len(obj))
	}
	// One property found. Check if it is a token.
	for k := range obj {
		v, ok := obj[k].(string)
		if !ok {
			return "", fmt.Errorf("property %s of type %s, expected string", k, reflect.TypeOf(obj[k]))
		}
		if strings.Contains(strings.ToLower(v), "token") {
			return v, nil
		}
	}
	return "", fmt.Errorf("no token found")
}

func (t *Token) GetTokenClaims(token string) (interface{}, error) {
	return t.GetTokenClaimsOpt(token, false)
}

func (t *Token) GetTokenClaimsUnsafe(token string) (interface{}, error) {
	return t.GetTokenClaimsOpt(token, true)
}

func (t *Token) GetTokenClaimsOpt(token string, unsafe bool) (interface{}, error) {
	// Get the claims from token.
	var resp fakeClaims
	if unsafe {
		if err := t.UnmarshalJWTUnsafe(token, &resp); err != nil {
			return nil, fmt.Errorf("error unmarshaling claims: %w", err)
		}
	} else {
		if err := t.UnmarshalJWT(token, &resp); err != nil {
			return nil, fmt.Errorf("error unmarshaling claims: %w", err)
		}
	}
	return resp, nil
}
