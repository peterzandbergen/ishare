package token

import (
	"testing"

	"gitlab.com/peterzandbergen/ishare/pkg/claims"
)

const (
	cbsEori = "EU.EORI.NL51197073"

	certCBS     = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem"
	keyCBS      = "/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem"
	caCertChain = "/home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem"
	rootCert    = "/home/peza/Documents/iShare/iShareTestKey2/root.pem"

	schemeOwnerHost = "https://scheme.isharetest.net"
	schemeOwnerEori = "EU.EORI.NL000000000"

	warehouse13Host = "https://w13.isharetest.net"
	warehouse13Eori = "EU.EORI.NL000000003"

	authorizationRegistryHost = "https://ar.isharetest.net"
	authorizationRegistryEori = "EU.EORI.NL000000004"
)

func TestNewFromFiles(t *testing.T) {
	ct, err := NewFromFiles(cbsEori, certCBS, keyCBS, caCertChain, rootCert)
	if err != nil {
		t.Fatalf("error generating token: %s", err)
	}
	ca, err := ct.Assertion(schemeOwnerEori)
	if err != nil {
		t.Fatalf("error creating client assertion: %s", err)
	}
	t.Logf(("%s"), ca)
}

type MyClaims struct {
	claims.Claims
	Payload string `json:"payload"`
}

func (m *MyClaims) GetClaims() claims.Claims {
	return m.Claims
}

func TestResponseToken(t *testing.T) {
	ct, err := NewFromFiles(cbsEori, certCBS, keyCBS, caCertChain, rootCert)
	if err != nil {
		t.Fatalf("error generating token: %s", err)
	}
	myClaims := MyClaims{
		Claims:  ct.NewClaims(schemeOwnerEori),
		Payload: "dit is mijn payload",
	}
	tk, err := ct.ResponseToken(&myClaims)
	if err != nil {
		t.Fatalf("error creating token: %s", err)
	}

	t.Logf("token: %s", tk)

	// try to unmarshal the token.
	var tt MyClaims
	err = ct.UnmarshalJWT(tk, &tt)
	if err != nil {
		t.Fatalf("error unmarshalling token: %s", err)
	}
}

func TestResponseTokenNoCA(t *testing.T) {
	ct, err := NewFromFiles(cbsEori, certCBS, keyCBS, caCertChain, rootCert)
	if err != nil {
		t.Fatalf("error generating token: %s", err)
	}
	// Clear ca chain
	ct.CaChainCerts = nil
	myClaims := MyClaims{
		Claims:  ct.NewClaims(schemeOwnerEori),
		Payload: "dit is mijn payload",
	}
	tk, err := ct.ResponseToken(&myClaims)
	if err != nil {
		t.Fatalf("error creating token: %s", err)
	}

	t.Logf("token: %s", tk)

	// try to unmarshal the token.
	var tt MyClaims
	err = ct.UnmarshalJWT(tk, &tt)
	if err != nil {
		t.Fatalf("error unmarshalling token: %s", err)
	}
}
