#!/bin/bash

go run ishare/cmd/ishareadapter \
    --client-eori    EU.EORI.NL51197073 \
    --client-cert    /home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem \
    --client-key     /home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem \
    --client-cacerts /home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem \
    --client-roots   /home/peza/Documents/iShare/iShareTestKey2/root.pem \
    --listen-address ":8080" \
    $*
