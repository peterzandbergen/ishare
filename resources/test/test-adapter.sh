#!/bin/bash

curl http://localhost:8080 \
    -H 'X-IShare-Server-Resource: https://scheme.isharetest.net/trusted_list' \
    -H 'X-IShare-Server-Host: https://scheme.isharetest.net' \
    -H 'X-IShare-Server-EORI: EU.EORI.NL000000000' \
    -H 'Accept: application/json' \
    -H 'X-Ishare-Adapter-Key: qwerty' \
    $*
