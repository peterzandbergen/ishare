#!/bin/bash

CUR_DIR=$(dirname $0)

$CUR_DIR/cbs-call.sh get \
    --server-eori=EU.EORI.NL000000000 \
    --token-url=https://scheme.isharetest.net \
    $*