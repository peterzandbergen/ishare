#!/bin/bash

CUR_DIR=$(dirname $0)

$CUR_DIR/cbs-call.sh trusted-list \
    --server-eori=EU.EORI.NL000000000 \
    --server-host=https://scheme.isharetest.net \
    $*