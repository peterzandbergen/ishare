#!/bin/bash

# 15 minutes is 
INTERVAL=300

while true
do
    ./modality.sh token \
        --server-eori=EU.EORI.NL000000000 \
        --token-url="https://bctn.modalityservices.nl:50333/oauth2.0/token"
    sleep $INTERVAL
done

