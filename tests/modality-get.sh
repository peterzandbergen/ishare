#!/bin/bash

CUR_DIR=$(dirname ${BASH_SOURCE[0]})

HOST_ADDRESS="bctn-ishare.modality.nl"

$CUR_DIR/modality.sh get \
    --server-eori=EU.EORI.NL000000000 \
    --token-url="https://${HOST_ADDRESS}/oauth2.0/token" \
    $*
