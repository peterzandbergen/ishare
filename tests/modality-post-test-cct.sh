# ./modality.sh post \
#     --server-eori=EU.EORI.NL000000000 \
#     --token-url="https://bctn.modalityservices.nl:50333/oauth2.0/token" \
#     --service-url="https://bctn.modalityservices.nl:50333/cbs/data" \
#     --data @modality-request.json \
#     --log-env dev \
#     $*

HOST_ADDRESS="cct-ishare.modality.nl"

CUR_DIR=$(dirname $0)

SERVER_EORI="EU.EORI.NL809245383"

$CUR_DIR/modality.sh post \
    --server-eori=${SERVER_EORI} \
    --token-url="https://${HOST_ADDRESS}/oauth2.0/token" \
    --service-url="https://${HOST_ADDRESS}/cbs/data" \
    --data @$CUR_DIR/modality-request.json \
    --log-env dev \
    $*
