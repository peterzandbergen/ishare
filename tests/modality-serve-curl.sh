# ./modality.sh post \
#     --server-eori=EU.EORI.NL000000000 \
#     --token-url="https://bctn.modalityservices.nl:50333/oauth2.0/token" \
#     --service-url="https://bctn.modalityservices.nl:50333/cbs/data" \
#     --data @modality-request.json \
#     --log-env dev \
#     $*

HOST_ADDRESS="bctn-ishare.modality.nl"

CUR_DIR=$(dirname $0)

# $CUR_DIR/modality.sh serve \
#     --log-env prod \
#     $*


# curl for testing
# X-Ishare-Adapter-Key: some-very-secret-key
# X-Ishare-Server-Host: scheme.ishare.net
# X-Ishare-Server-Eori: 00000000001
# X-Ishare-Server-Resource: http://scheme.ishare.net/capabilities

# https://bctn-ishare.modality.nl/cbs/data"

SERVER_EORI="EU.EORI.NL809245383"

curl -v -H "X-Ishare-Server-Eori: ${SERVER_EORI}" \
    -H 'X-Ishare-Service-Url: https://bctn-ishare.modality.nl/cbs/data' \
    -H 'X-Ishare-Token-Url: https://bctn-ishare.modality.nl/oauth2.0/token' \
    -H 'Content-Type: application/json' \
    -d '{"tripStartDate":"2020-09-29","tripEndDate":"2020-09-30","page": 4}' \
    http://localhost:8080