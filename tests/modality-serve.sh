# ./modality.sh post \
#     --server-eori=EU.EORI.NL000000000 \
#     --token-url="https://bctn.modalityservices.nl:50333/oauth2.0/token" \
#     --service-url="https://bctn.modalityservices.nl:50333/cbs/data" \
#     --data @modality-request.json \
#     --log-env dev \
#     $*

HOST_ADDRESS="bctn-ishare.modality.nl"

CUR_DIR=$(dirname $0)

$CUR_DIR/modality.sh serve \
    --log-env prod \
    $*


# curl for testing
# X-Ishare-Adapter-Key: some-very-secret-key
# X-Ishare-Server-Host: scheme.ishare.net
# X-Ishare-Server-Eori: 00000000001
# X-Ishare-Server-Resource: http://scheme.ishare.net/capabilities

# https://bctn-ishare.modality.nl/cbs/data"

# curl -h 'X-Ishare-Server-Host: bctn-ishare.modality.nl' -H 'X-Ishare-Server-Eori: '  -H 'X-Ishare-Token-Url: https://bctn.modalityservices.nl:50333/cbs/data'