#!/bin/bash

# ./modality.sh post \
#     --server-eori=EU.EORI.NL000000000 \
#     --token-url="https://bctn.modalityservices.nl:50333/oauth2.0/token" \
#     --service-url="https://bctn.modalityservices.nl:50333/cbs/data" \
#     --data @modality-request.json \
#     --log-env dev \
#     $*

HOST_ADDRESS="bctn-ishare.modality.nl"

CUR_DIR=$(dirname $0)
ISHARE_SERVER_EORI=EU.EORI.NL809245383

echo Requesting access token for client $ISHARE_CLIENT_EORI to access $ISHARE_SERVER_EORI ...
$CUR_DIR/modality.sh token \
    --token-url="https://${HOST_ADDRESS}/oauth2.0/token" \
    --server-eori=$ISHARE_SERVER_EORI \
    --log-env dev \
    $*
