#!/bin/bash

CUR_DIR=$(dirname $0)

$CUR_DIR/modality.sh token \
    --server-eori=EU.EORI.NL000000000 \
    --token-url="https://bctn.modalityservices.nl:50333/oauth2.0/token" \
    $*
