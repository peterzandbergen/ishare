#!/bin/bash

CMD="go run ishare/cmd/isharecli"
# CMD='podman run --rm -it localhost/test-location/isharecli:latest'
# CMD="env"

export ISHARE_CLIENT_EORI="EU.EORI.NL51197073"
export ISHARE_CLIENT_KEY=/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.key.pem
export ISHARE_CLIENT_CERT=/home/peza/Documents/iShare/iShareTestKey2/pcl.zandbergen.crt.pem
export ISHARE_CLIENT_CACERTS=/home/peza/Documents/iShare/iShareTestKey2/crt-chain.pem
export ISHARE_CLIENT_ROOTS=/home/peza/Documents/iShare/iShareTestKey2/root.pem

# --server-eori=12345 
# --token-url=https://bctn.modalityservices.nl:50333/oauth2.0/token

# date
$CMD $@ 

# Examples

# bash modality.sh assertion --server-eori=EU.EORI.NL000000000
# bash modality.sh token --server-eori=EU.EORI.NL000000000 --token-url="https://bctn.modalityservices.nl:50333/oauth2.0/token"